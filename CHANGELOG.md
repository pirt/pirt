# v0.5
- Initial implementaion of 3D solve

# v0.4
- Doxygen documentation generation is now enabled!
- Enhanced tests with python tests to analyze outputs for consistency and accuracy.
- Cleaned up reposity, move unrelated files to auxilary repository.

# v0.3
- Added a pirtctx struct to simplify data management

# v0.2.2
- Added a convolve interface
- Removed redundant tao solver queries, this information can instead be obtained using `-tao_view`

# v0.2.1
- Added fft profiling stage

# v0.2
- Moved all I/O to HDF5
- Enchanced logging for studying performance.

# v0.1.1
- Fixed bug in alternating solver `solve_alt`. 
- Enchanced logging for studying performance.

# v0.1
Known to work with joint solver `solve`.

