Basic architecture
==================

This file describes the basic architecute of PIRT when solving for one
slice on one MPI sub-communicator. For the purpose of this document,
only the relevant members for the aforementioned (1 slice on 1 subcomm)
are described in detail.

2D algorithm
------------

The basic 2D algorithm implemented in PIRT is based upon [this
paper](https://doi.org/10.1137/18M121993X). The tomography projection
operator is discretized as per the line model and the center-of-rotation
errors are accounted for by a gaussian convolution. The task of solving
for the object is posed as a minization problem with the mismatch
between the simulated data and the shifted experimental data as the cost
function. The shifts here account for the center of rotation errors.

Implementation Details
----------------------

The two primary data structures used are described in `pirtctx.hpp`, the
global and local context objects. When performing one concurrent solve
(1 MPI sub-communicator), both of these objects live on the same MPI
communicator. The `pirt.cxx` file contains the `main` function which
does the following :

-   process input options, load data, setup FFTW plans and allocate
    space (`utils.cxx` & `io.cxx`)
-   generate the tomography projection matrix (`matrix.cxx` &
    `geom.cxx`)
-   setup TAO object describing the minimization problem and execute
    (`solver(_alt/_joint).cxx`)
-   smooth the experimental data by convolving with gaussian
    (`convolve.cxx`)
-   save results and free memory (`utils.cxx` & `io.cxx`)

Data Distribution
-----------------

The data distribution is as follows : when dealing with an object of
size `ntau x ntau` and `ntheta` projections, the sinogram shape is
`ntau x ntheta` and the dimensions of the projection matrix are 
`ntau x ntheta` (rows) by `ntau x ntau` (columns). When using the 
joint algorithm, the optimization vector has dimensions
`ntau x ntau + ntheta`. Since `PETSc` distributes matrices by rows
(each MPI rank gets ownership of a subset of rows), we use a simple 
heuristic to assign a set of projection angles, `ntheta_local` to
each MPI rank thereby assigning `ntheta_local x ntau` rows to that
MPI rank. This distribution is necessary because the shifts
correction is performed by performing a convolution for each 
projection angle and we don't want the data from one projection
angle to live on different MPI ranks.

![image](images/2d-dist.png)
