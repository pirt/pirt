Subcomm architecture
====================

This file describes the architecture of PIRT when solving multiple
slices on multiple MPI sub-communicators. For the basic architecture of
PIRT refer to the [basic architecture page](../2D-solver-architecture).

Subcomm basics
--------------

To execute multiple reconstructions concurrently, PIRT uses `PETSc`
subcommunicators which are based upon MPI subcommunicators but include
some utilities to simplify usage. The global MPI communicator is split
into task subcomms and each task subcomm works on one tomography slice
at any given time. To manage the distribution of slices over task
subcomms (in other words to decide which task subcomm works on which
slice at any given time), an organizer subcomm is also created. The
schematic below illustrates this process. Each task subcomm is given a
unique identifier, the highest MPI rank in the global MPI communicator
of all the constituent ranks.

![image](images/subcomms.png)

The organizer subcomm is created by combining all MPI ranks whose rank
in the task subcomm is 0 (though it's rank on global rank may not be 0)
and a `PETSc` index set is then defined on this subcomm. The indices
that are assigned to each rank (on this organzier subcomm) are taken to
be the indices (or slice indices) that the associated task subcomm
reconstructs.

Sync options
------------

When solving multiple slices concurrently, there is an option to combine
the estimates of shifts from each task subcomm into a global shifts
vector (either by taking the mean or approximating the median) and
distributing this back to the task subcomms. The schematic for doing
this by taking the mean of shifts on subcomms is shown below.

![image](images/sync.png)
