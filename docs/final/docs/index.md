# Overview
**Parallel Iterative Reconstruction for Tomography** - PIRT - is a tomography inversion code that incorporates center of rotation correction written with the goal of being scalable on HPC systems. PIRT is built upon PETSc (which handles the optimization routines, data management and parallel I/O), Boost (which handles geometry routines) and FFTW (for position correction cost function evaluation).

## Building PIRT & dependencies
For more information on building PIRT and it's dependencies, see [the installation documentation](https://pirt.gitlab.io/pirt/installation/).

## Quickstart
For a basic demo on PIRT, [see the quickstart page](https://pirt.gitlab.io/pirt/quickstart/).

## Running PIRT
For more information on running PIRT, see [the page on running PIRT](https://pirt.gitlab.io/pirt/running/). This page includes detailed information on the input data format required by PIRT. 

## Implementation details & architecture
Information on PIRT's implementation and details and architecture can be found at the [basic](https://pirt.gitlab.io/pirt/singlearch) and [advanced](https://pirt.gitlab.io/pirt/subcommarch) pages. Furthermore, Doxygen manual pages for PIRT are available [here](https://pirt.gitlab.io/pirt/doxygen-docs/)

## Testing
Information on tests employed in PIRT's CI can be found at the [test description page](https://pirt.gitlab.io/pirt/testing/).
