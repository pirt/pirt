Installation
============

Build System
------------

PIRT uses [PETSc's application Makefile](https://petsc.org/release/docs/manual/getting_started/#writing-application-codes-with-petsc) for building.

Dependencies
------------

PIRT has the following dependencies :

    PETSc+hdf5
    Boost-Geometry
    FFTW
    HDF5

where the `PETSc+hdf5` means that `PETSc` must be built with hdf5 support. Since `boost-geometry` and `FFTW` are not included as `PETSc` dependencies, the link flags and include locations are also specified. The tedious task of installing the dependencies and generating the correct include locations is simplified by the use of [spack-environments](https://spack.readthedocs.io/en/latest/environments.html). A quickstart for spack can be found [here](https://spack-tutorial.readthedocs.io/en/latest/). Note that the makefile assumes that the [view](https://spack.readthedocs.io/en/latest/environments.html#environment-managed-views) associated with the spack-environment is at the default location or at `/opt/view` which is the default location for the view in container recipies [generated by spack](https://spack.readthedocs.io/en/latest/containers.html). If the view is at a different location, please update the definition of the `SPACK_VIEW` in the makefile accordingly. The usage of `spack` is not necessary and if one chooses to install the dependencies by some other package manager, the `INCFILES` and `LIBFILES` variables need to set appropriately. 

An example spack environment file which lists all the dependencies is shown below :

    # This is a Spack Environment file.
    #
    # It describes a set of packages to be installed, along with
    # configuration settings.
    spack:
      packages:
        all:
          providers:
            mpi:
            - mpich
            blas:
            - openblas
            lapack:
            - openblas
      specs:
       - petsc@3.16.1 +hdf5+mpi
       - hdf5@1.12.1 +mpi+hl
       - fftw~mpi
       - boost cxxstd=17
      concretization: together
      view: true

Note that `spack` can [re-use existing installations](https://spack.readthedocs.io/en/latest/build_settings.html#external-packages) of packages to speed-up compilcation times, among other uses. Installing all packages from the source code typically takes under an hour on a modern server processor. 

Container
----------

A docker-container with all the dependencies installed is avaialble [here](https://gitlab.com/pirt/pirt-container/container_registry)


Building pirt
-------------

With the dependencies installed, the main executable for PIRT can be installed by :

    cd src/ && make pirt

and one can optionally add `-j n` to have n processes in parallel and `-B` to trigger recompilation of all files.
