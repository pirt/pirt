Running PIRT
============

Input format
------------

PIRT requies two input files in the HDF5 format, one containing the
angles and one containing the sinograms. The anlgefile should contain a
group "theta" with a dataset "angles" as shown below (`h5glance` output):


    anglefile.h5
    └theta
      └angles       [float64: 100]

and the sinofile should contain a groups `sinon` for each of the
sinograms where `n` is the index of the sinogram `{0..N-1}` (where `N`
refers to the number of slices) each with a dataset "sinogram" containing
the actual data as shown below (`h5glance` output):

    sinofile.h5
    |sino0
    │ └sinogram  [float64: 25600]
    ├sino1
    │ └sinogram  [float64: 25600]
    ......

To test on synthetic data, the sinofile must contain a single dataset
`input` in the `sample` group. One would then have to add the `-synthetic` 
flag. With the synthetic sample supplied, PIRT generates a sinogram 
using a tomography projection matrix and solves the inverse problem 
as usual. The synthetic mode of reconstruction is mainly used for
testing and limited to one concurrent solve.

Output format
-------------

PIRT generates HDF5 files for each reconstructed slice with the name
`sol_n.h5` with `n` indicating the index. The structure of the file is 
shown below (`h5glance` output):

    sol_0.h5
    ├cor
    │ ├shifts       [float64: 150]
    │ └sinogram     [float64: 76800]
    └reconstruction
      └solution     [float64: 262144]

where the `reonstruction/solution` contains the reconstructed sample, 
the `cor/shifts` contains the estimated center of rotationd drifts and 
the `cor/sinogram` contains the corrected sinogram respectively.

Running PIRT
------------

PIRT can be run by invoking the mpi launcher as shown below :

    mpirun  -np num_mpi_ranks ./pirt \
            -runtime-option value

Runtime options
---------------

An exhaustive list of PIRT\'s runtime options is given below :

| Option              | Default | Description                                              |
| ------------------- | ------- | -------------------------------------------------------- |
|  `ntau`             |   N/A   | :orange_square: size of the object                       |
|  `ntheta`           |   N/A   | :orange_square: number of projection anlges              |
|  `anglefile`        |   N/A   | :orange_square: hdf5 file containing projection angles   |
|  `sinofile`         |   N/A   | :orange_square: hdf5 file containing sinograms           |
|  `nslices`          |    1    | :purple_circle: number of 3D slices                      |
|  `nsubcomms`        |    1    | :purple_circle: number of concurrent solves              |
|  `overall_sweeps`   |    1    |   number of sweeps over all slices                       |
|  `alt_outer_its`    |   15    |   number of outer iterations for alternating solve       |
|  `alt_sample_its`   |    5    |   number of (inner) iterations for sample solve          |
|  `alt_shifts_its`   |    2    |   number of (inner) iterations for shifts solve          |
|  `joint_its`        |   100   |   number of iterations for joint solve                   |
|  `matfile`          |  N/A    |   petsc binary file containing the projection matrix     |
|  `joint`            |  FALSE  |   enable joint CoR error correction                      |
|  `alternating`      |  FALSE  |   enable alternating CoR error correction                |
|  `regularize`       |  FALSE  |   enable adaptive L1 regularization                      |
|  `combine_mean`     |  FALSE  |   combine shifts from concurrent solves via their mean   |
|  `combine_median`   |  FALSE  |   combine shifts from concurrent solves via their median |
|  `synthetic`        |  FALSE  |   generate sinogram from sample before reconstruction    |

The options which include a :orange_square: are necessary arguments for a 2D solve and the
options which include a :purple_circle: are necessary for a 3D solve. In addition to 
the above, PIRT also accepts any relevant `PETSc` command line options like `-log_view`
for detailed performance logs.

Some extra runtime options used for confidence checks are :

| Option              | Description                                                  |
| ------------------- | ------------------------------------------------------------ |
| `exactshiftsfile`   | specify a file containg known shifts, for confidence checks  | 
| `exact_shifts`      | use the exact shifts as given by the above option            |
| `normalize_mat`     |  normalize the tomography matrix after generating it         |
