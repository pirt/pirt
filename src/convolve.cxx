/*! \file convolve.cxx
 * Routines for CoR alignment.
 */

#include "convolve.hpp"

/* --------------------------------------------------------------------- */
/*!
  Generate aligned data (i.e. correct for center of rotiaon
  error) using gaussian smoothing.

  Input Parameter:
  \param shifts_vec - PETSc vector containing the shifts
  \param pirt_l     - pirt local context
  \return ierr      - PetscErrorCode

*/
PetscErrorCode convolve(Vec shifts_vec, pirt_local_ctx *pirt_l) {

  PetscInt          data_lowidx, data_highidx;
  PetscInt          shifts_lowidx, shifts_highidx;
  PetscInt          theta_idx, tau_idx;
  PetscInt          local_size_vec;
  PetscInt          global_i, i;
  PetscScalar const *shifts_array;
  PetscScalar       *b_array;
  PetscScalar const *x_array;
  PetscReal         delay, range;
  PetscReal         sigma, scale;
  PetscReal         c_a, c_b, c_c, c_d;
  PetscErrorCode    ierr;

  PetscFunctionBeginUser;

  ierr = VecGetOwnershipRange(pirt_l->expdata, &data_lowidx, &data_highidx);  CHKERRQ(ierr);
  ierr = VecGetLocalSize(pirt_l->expdata, &local_size_vec);  CHKERRQ(ierr);
  ierr = VecGetArrayRead(pirt_l->expdata, &x_array);CHKERRQ(ierr);

  ierr = VecGetOwnershipRange(shifts_vec, &shifts_lowidx, &shifts_highidx);  CHKERRQ(ierr);
  ierr = VecGetArrayRead(shifts_vec, &shifts_array);CHKERRQ(ierr);

  sigma = 2.0/2.355; /* Sigma in Gaussian */
  scale = 1/sqrt(2*PETSC_PI)/sigma; /* Prefactor to have a normal distribution */

  for (i=0; i<local_size_vec; i++) {

    global_i  = i + data_lowidx;
    theta_idx = (global_i)/pirt_l->ntau;
    tau_idx   = (global_i)%pirt_l->ntau;

    delay     = shifts_array[theta_idx - shifts_lowidx];
    pirt_l->data_b[i][0] = x_array[i]; /* Real part */
    pirt_l->data_b[i][1] = 0;          /* Imag part */

    if (tau_idx > (pirt_l->ntau/2 - 1)) {
      range = tau_idx - pirt_l->ntau;
    } else {
      range = tau_idx;
    }
    /* Define Gaussian */
    pirt_l->data_g[i][0] = scale*exp(-pow((range-delay),2)/(2*pow(sigma,2))); /* Real part */
    pirt_l->data_g[i][1] = 0;                                                 /* Imag part */
  }
  ierr = VecRestoreArrayRead(pirt_l->expdata,&x_array);CHKERRQ(ierr);

  /* Take FFTs */
  fftw_execute(pirt_l->fplan_b);
  fftw_execute(pirt_l->fplan_g);

  /* Pointwise multiplication : (a+bi)*(c+di) = (ac - bd) + (bc + ad) i */
  for (i=0; i<local_size_vec; i++) {
    c_a = pirt_l->data_b[i][0];
    c_b = pirt_l->data_b[i][1];
    c_c = pirt_l->data_g[i][0];
    c_d = pirt_l->data_g[i][1];
    /* printf("fft(b[ %d ]): %e %e\n",i+data_lowidx,pirt_l->data_b[i][0],pirt_l->data_b[i][1]); */
    /* We use data_g as a temporary buffer
       We don't want to overwrite data_b, since
       we still want the ft of data_b later */
    pirt_l->data_g[i][0] = c_a*c_c - c_b*c_d;
    pirt_l->data_g[i][1] = c_b*c_c + c_a*c_d;
  }

  /* Inverse FFT */
  fftw_execute(pirt_l->bplan_g);

  /* Copy into b and normalize , This gives us alignedSignal */
  ierr = VecGetArray(pirt_l->smoothed_data, &b_array);CHKERRQ(ierr);
  for (i=0; i<local_size_vec; i++) {
    b_array[i] = pirt_l->data_g[i][0]/pirt_l->ntau; /* smoothed_data */
  }
  ierr = VecRestoreArray(pirt_l->smoothed_data, &b_array);CHKERRQ(ierr);

  /* Now get DalignedSignal */
  for (i=0; i<local_size_vec; i++) {
    global_i  = i + data_lowidx; /* offset */
    theta_idx = (global_i)/pirt_l->ntau;
    tau_idx   = (global_i)%pirt_l->ntau;
    delay     = shifts_array[theta_idx - shifts_lowidx];
    if (tau_idx > (pirt_l->ntau/2-1)){
      range = tau_idx-pirt_l->ntau;
    } else {
      range = tau_idx;
    }
    /* Define gradient of gaussian wrt delay */
    pirt_l->data_g[i][0] = scale*((range-delay)/(pow(sigma,2))*exp(-pow((range-delay),2)/(2*pow(sigma,2)))); /* Real part */
    pirt_l->data_g[i][1] = 0;                                                                                /* Imag part */
  }
  ierr = VecRestoreArrayRead(shifts_vec, &shifts_array);CHKERRQ(ierr);

  /* Take FFTs */
  fftw_execute(pirt_l->fplan_g);

  /* Pointwixe multiplication */
  /* (a+bi)*(c+di) = (ac - bd) + (bc + ad) i */
  for (i=0; i<local_size_vec; i++) {
    c_a = pirt_l->data_b[i][0];
    c_b = pirt_l->data_b[i][1];
    c_c = pirt_l->data_g[i][0];
    c_d = pirt_l->data_g[i][1];
    /* We use data_g as a temporary buffer */
    /* We don't want to overwrite data_b, since we still want the fft of data_b later */
    pirt_l->data_g[i][0] = c_a * c_c - c_b * c_d;
    pirt_l->data_g[i][1] = c_b * c_c + c_a * c_d;
  }

  /* Inverse FFT */
  fftw_execute(pirt_l->bplan_g);
  /* This gives us DalignedSignal in data_g */

  PetscFunctionReturn(ierr);
}
