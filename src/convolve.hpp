#ifndef CONVOLVE_H
#define CONVOLVE_H

#include "pirtctx.hpp"

PetscErrorCode convolve(Vec shifts, pirt_local_ctx *pirt_p);

#endif
