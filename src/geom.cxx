/*! \file geom.cxx
 * Geometry routines used for creating the tomography projection matrix
 */

#include "geom.hpp"
#include <vector>

/* --------------------------------------------------------------------- */
/*!
  Return a vector of linearly spaced values.

  Input Parameter:
  \param a   - Start
  \param b   - Stop
  \param N   - Number of elements
  \return xs - Vector with linearly spaced values between a and b with size N

*/
std::vector<double> linspace(double a, double b, size_t N) {
  double h = (b - a) / static_cast<double>(N-1);
  std::vector<double> xs(N);
  typename std::vector<double>::iterator x;
  double val;
  for (x = xs.begin(), val = a; x != xs.end(); ++x, val += h){
    *x = val;
  }
  return xs;
}

/* --------------------------------------------------------------------- */
/*!
  Helper routine for sorting an array of points

  Input Parameter:
  \param p     - input point
  \param q     - input point
  \return bool - comparision result

*/
bool point_comparison(const point_t& p, const point_t& q){
  return p.get<0>()<q.get<0>();
}

/* --------------------------------------------------------------------- */
/*!
  Initialize members of the struct detctx (prior to creating the
  tomography projection matrix)

  \param *pirt_g  - pirt_context
  \param *det     - pointer to detector geometry struct
  \return ierr    - PetscErrorCode

*/
PetscErrorCode init_detctx(detctx *det, pirt_global_ctx *pirt_g){
  double              alpha,Tau,tol1;
  point_t             detS0,detE0,SourceS0,SourceE0;
  std::vector<double> knot;
  PetscErrorCode      ierr;

  PetscFunctionBeginUser;

  if (pirt_g->debug) {
    ierr = PetscPrintf(PETSC_COMM_SELF,"At detector geom initialization : det.m: %d & %d \n", (PetscInt) det->m[0], (PetscInt) det->m[1]);
    CHKERRQ(ierr);
  }

  /* angle of diagonal */
  alpha = atan((det->omega[3] - det->omega[2])/(det->omega[1] - det->omega[0]));
  Tau   = det->omega[1] - det->omega[0];
  det->dz[0] = (det->omega[1] - det->omega[0])/(det->m[1]);
  det->dz[1] = (det->omega[3] - det->omega[2])/(det->m[0]);
  tol1       = 0 * det->m[0];

  detS0 = point_t(Tau/2 *tan(alpha) + tol1*det->dz[0], -Tau/2 - tol1*det->dz[0]);
  detE0 = point_t(Tau/2 *tan(alpha) + tol1*det->dz[0],  Tau/2 + tol1*det->dz[0]);
  knot  = linspace(detS0.get<1>(),detE0.get<1>(),det->ntau);

  for (int i=0; i<(int)knot.size(); i++) {
    det->DetKnot0.push_back(point_t(detS0.get<0>(),knot[i]));
  }

  SourceS0 = point_t(-Tau/2 *tan(alpha) - tol1*det->dz[0], -Tau/2 - tol1*det->dz[0]);
  SourceE0 = point_t(-Tau/2 *tan(alpha) - tol1*det->dz[0],  Tau/2 + tol1*det->dz[0]);

  /* # of nonzeros = |SourceS0-SourceE0|/m[0] */
  knot = linspace(SourceS0.get<1>(),SourceE0.get<1>(),det->ntau);
  for (int i=0; i<(int)knot.size(); i++) {
    det->SourceKnot0.push_back(point_t(SourceS0.get<0>(),knot[i]));
  }

  det->x = linspace(det->omega[0],det->omega[1],det->m[0]+1);
  det->y = linspace(det->omega[2],det->omega[3],det->m[1]+1);

  bg::append(det->rectangle, point_t(det->omega[0],det->omega[2]));
  bg::append(det->rectangle, point_t(det->omega[0],det->omega[3]));
  bg::append(det->rectangle, point_t(det->omega[1],det->omega[3]));
  bg::append(det->rectangle, point_t(det->omega[1],det->omega[2]));
  bg::append(det->rectangle, point_t(det->omega[0],det->omega[2]));

  if (pirt_g->debug) {
    ierr = PetscPrintf(PETSC_COMM_SELF,"At detector geom initialization : det.x.size: %d, det.y.size: %d, det.m: %d & %d, \n",
        (PetscInt) det->x.size(), (PetscInt) det->y.size(), (PetscInt) det->m[0], (PetscInt) det->m[1]);CHKERRQ(ierr);
  }
  PetscFunctionReturn(0);
}

/* --------------------------------------------------------------------- */
/*!
  Fill the tomography projection matrix


  Input Parameter:
  \param tau_idx   - index of current location within a projection
  \param theta_idx - index of current projection angle
  \param *det      - detector geometry context
  \param *pirt_g   - pirt global context
  \param *pirt_l   - pirt local context
  \return ierr     - PetscErrorCode

*/
PetscErrorCode setMatrixElements(PetscInt tau_idx, PetscInt theta_idx, detctx *det, pirt_global_ctx *pirt_g, pirt_local_ctx *pirt_l){
  PetscErrorCode  ierr;
  PetscReal       slope,intercept,box_bound;
  PetscScalar     theta;
  PetscScalar     thetaread;
  PetscScalar     *vals;
  PetscInt        *colidx;
  PetscInt        rowidx;
  PetscInt        numvals;
  PetscInt        allocsize;

  linestring_t   line;
  linestring_t   A;
  point_t        pt_rot1,pt_rot2, pt_Q;

  std::vector<point_t>  Q;

  double _x, _y;

  int x_ind,y_ind;
  int i,j,size;
  int j_mat;

  PetscFunctionBeginUser;

  ierr = VecGetValues(pirt_l->exptheta, 1, (PetscInt const*) &theta_idx, &thetaread);CHKERRQ(ierr);
  theta = (thetaread) * (PETSC_PI/180.0);

  /* Rotate point 1 of line */
  pt_rot1 = point_t( cos(theta) * det->SourceKnot0[tau_idx].get<0>() - sin(theta) * det->SourceKnot0[tau_idx].get<1>(),
      sin(theta) * det->SourceKnot0[tau_idx].get<0>() + cos(theta) * det->SourceKnot0[tau_idx].get<1>());

  /* Rotate point 2 of line */
  pt_rot2 = point_t( cos(theta) * det->DetKnot0[tau_idx].get<0>() - sin(theta) * det->DetKnot0[tau_idx].get<1>(),
      sin(theta) * det->DetKnot0[tau_idx].get<0>() + cos(theta) * det->DetKnot0[tau_idx].get<1>());

  /* Define lines that are always bigger than the rectangle */
  box_bound = det->omega[0];
  if (fabs(pt_rot1.get<0>() - pt_rot2.get<0>())<1e-14) {
    /* Check if line is vertical
       Use (-2w,x) and (2w,x) as our points */
    pt_rot1 = point_t( pt_rot1.get<0>(),  2*box_bound);
    pt_rot2 = point_t( pt_rot1.get<0>(), -2*box_bound);
  } else {
    slope = (pt_rot1.get<1>() - pt_rot2.get<1>())/(pt_rot1.get<0>() - pt_rot2.get<0>());
    intercept = pt_rot1.get<1>() - slope*pt_rot1.get<0>();
    if (fabs(slope)>1) {
      /* We use x = (y-b)/m, and use y points as 2*box_bound and -2*box_bound */
      pt_rot1 = point_t( (2*box_bound - intercept) / slope, 2*box_bound);
      pt_rot2 = point_t( (-2*box_bound - intercept) / slope, -2*box_bound);
    }  else {
      /* We use y = mx + b and use x points as 2*box_bound and -2 * box_bound */
      pt_rot1 = point_t( 2*box_bound, slope*2*box_bound + intercept);
      pt_rot2 = point_t( -2*box_bound, -slope*2*box_bound + intercept);
    }
  }

  bg::append(line, pt_rot1);
  bg::append(line, pt_rot2);

  /* Find intersection */
  bg::intersection(line,det->rectangle,A);
  bg::unique(A);

  if (A.size()==0||A.size()==1){
    /* Do nothing */
  } else {
    if( theta==PETSC_PI/2){
      /* Only use first point in special cases because second point
         is grabbed from direction
         90 degrees - vertical line */
      Q.reserve((int)det->y.size());
      for (i=0; i<(int)det->y.size(); i++) {
        pt_Q = point_t(A[0].get<0>(),det->y[i]);
        Q.emplace_back(pt_Q);
      }
    } else if (theta==0 || theta==2*PETSC_PI) {
      /* 0, 360 - horizontal line */
      Q.reserve((int)det->x.size());
      for (i=0; i<(int)det->x.size(); i++) {
        pt_Q = point_t(det->x[i], A[0].get<1>());
        Q.emplace_back(pt_Q);
      }
    } else if (theta==PETSC_PI) {
      /* 180 - source, detector switched location */
      Q.reserve((int)det->x.size());
      for (i=(det->x.size()-1); i>=0; i--) {
        pt_Q = point_t(det->x[i], A[0].get<1>());
        Q.emplace_back(pt_Q);
      }
    } else if (theta==3*PETSC_PI/2) {
      /* 270 - source, detector switched location */
      Q.reserve((int)det->y.size());
      for (i=(det->y.size()-1); i>=0; i--) {
        pt_Q = point_t(A[0].get<0>(), det->y[i]);
        Q.emplace_back(pt_Q);
      }
    } else {
      Q.reserve((int)det->x.size() + (int)det->y.size());
      for (i=0;i<(int)det->x.size();i++) {
        _y = (A[1].get<1>() - A[0].get<1>())/(A[1].get<0>() - A[0].get<0>())*det->x[i]
          + (A[0].get<1>()*A[1].get<0>()- A[1].get<1>()*A[0].get<0>())/(A[1].get<0>() - A[0].get<0>());
        if ((_y > det->omega[2]) && (_y < det->omega[3])){
          pt_Q = point_t(det->x[i],_y);
          Q.emplace_back(pt_Q);
        }
      }
      for (i=0;i<(int)det->y.size();i++) {
        _x = (det->y[i] - (A[0].get<1>()*A[1].get<0>() - A[1].get<1>()*A[0].get<0>())/
            (A[1].get<0>() - A[0].get<0>()))/((A[1].get<1>() - A[0].get<1>())/(A[1].get<0>() - A[0].get<0>()));
        if ((_x > det->omega[0]) && (_x < det->omega[1])){
          pt_Q = point_t(_x,det->y[i]);
          Q.emplace_back(pt_Q);
        }
      }
    }

    if (pirt_g->debug) {
      j = 0;
      size = Q.size();
      for (i=0;i<size;i++) {
        if (!bg::covered_by(Q[j],det->rectangle)) {
          Q.erase(Q.begin() + j);
        } else {
          j++;
        }
      }
    }

    /* Sort for ascending x */
    std::sort(Q.begin(), Q.end(), point_comparison);
    numvals   = 0 ;
    allocsize = (PetscInt)Q.size();
    //ierr = PetscPrintf(PETSC_COMM_SELF, "rank: %d, allocsize: %d\n", pirt_l->rank, allocsize);
    ierr = PetscMalloc1(allocsize, &colidx);CHKERRQ(ierr);
    ierr = PetscMalloc1(allocsize, &vals);CHKERRQ(ierr);
    rowidx = theta_idx*det->ntau + tau_idx;

    /* Get distance between neighbors */
    for (i=0; i<(int)Q.size()-1; i++) {
      point_t tmp_point = point_t((Q[i].get<0>() + Q[i+1].get<0>())/2,
          (Q[i].get<1>() + Q[i+1].get<1>())/2);
      x_ind = (tmp_point.get<0>() - det->omega[0])/det->dz[0];
      y_ind = (tmp_point.get<1>() - det->omega[2])/det->dz[1];
      /* m[1] and m[0] are (confuPetscSinRealgly) correct */
      if (x_ind>=0 && x_ind<det->m[1] && y_ind>=0 && y_ind<det->m[0]) {
        j_mat = x_ind * det->m[0] + y_ind;
        vals[numvals]   = bg::distance(Q[i], Q[i+1]);
        colidx[numvals] = j_mat;
        numvals = numvals+1;
      }
    }

    ierr = MatSetValues(pirt_l->A, 1, &rowidx, numvals, colidx, vals, INSERT_VALUES);CHKERRQ(ierr);
    PetscFree(colidx);
    PetscFree(vals);

    /* todo :  Add Lvec unique here
       std::vector<int>     index;
       std::vector<double>  Lvec;
       auto last = std::unique(index.begin(), index.end());
       index.erase(last,index.end()); */
  }

  PetscFunctionReturn(0);
}

/* --------------------------------------------------------------------- */
/*!
  Count the number of elements for one row of the
  tomography projection matrix, used for preallocation of matrix.

  \param tau_idx   - index of current location within a projection
  \param theta_idx - index of current projection angle
  \param *det      - detector geometry context
  \param *pirt_g   - pirt global context
  \param *pirt_l   - pirt local context
  \param *d_nz     - number of non zeros in diagonal portion of matrix
  \param *o_nz     - number of non zeros in off-diagonal portion of matrix
  \return ierr     - PetscErrorCode

*/
PetscErrorCode countMatrixElements(PetscInt tau_idx, PetscInt theta_idx, detctx *det, pirt_global_ctx *pirt_g, pirt_local_ctx *pirt_l, PetscInt *d_nz, PetscInt *o_nz){

  PetscErrorCode ierr;
  PetscInt       c_Istart, c_Iend;
  PetscScalar    thetaread;

  linestring_t line;
  linestring_t A;
  point_t      pt_rot1, pt_rot2, pt_Q;

  std::vector<int>      index;
  std::vector<point_t>  Q;

  double      slope, intercept, box_bound;
  double      theta;

  double _x,_y;

  int x_ind,y_ind;
  int i,j,size;
  int j_mat;

  PetscFunctionBeginUser;

  Q.clear();
  ierr = MatGetOwnershipRangeColumn(pirt_l->A,&c_Istart,&c_Iend);CHKERRQ(ierr);

  ierr  = VecGetValues(pirt_l->exptheta, 1, (PetscInt const*) &theta_idx, &thetaread);CHKERRQ(ierr);
  theta = (double) (thetaread * (PETSC_PI/180.0));

  /* Rotate point 1 of line */
  pt_rot1 = point_t(cos(theta) * det->SourceKnot0[tau_idx].get<0>() - sin( theta) * det->SourceKnot0[tau_idx].get<1>(),
      sin(theta) * det->SourceKnot0[tau_idx].get<0>() + cos( theta) * det->SourceKnot0[tau_idx].get<1>());

  /* Rotate point 2 of line */
  pt_rot2 = point_t( cos(theta) * det->DetKnot0[tau_idx].get<0>() - sin(theta)*det->DetKnot0[tau_idx].get<1>(),
      sin(theta) * det->DetKnot0[tau_idx].get<0>() + cos(theta) * det->DetKnot0[tau_idx].get<1>());

  /* Define lines that are always bigger than the rectangle */
  box_bound = det->omega[0];
  if (fabs(pt_rot1.get<0>() - pt_rot2.get<0>())<1e-14) {
    /* Check if line is vertical
       Use (-2w,x) and (2w,x) as our points */
    pt_rot1 = point_t( pt_rot1.get<0>(),  2*box_bound );
    pt_rot2 = point_t( pt_rot1.get<0>(), -2*box_bound );
  } else {
    slope = (pt_rot1.get<1>() - pt_rot2.get<1>())/(pt_rot1.get<0>() - pt_rot2.get<0>());
    intercept = pt_rot1.get<1>() - slope*pt_rot1.get<0>();
    if (fabs(slope)>1){
      //We use x = (y-b)/m, and use y points as 2*box_bound and -2*box_bound
      pt_rot1 = point_t( ( 2*box_bound - intercept) / slope,  2*box_bound);
      pt_rot2 = point_t( (-2*box_bound - intercept) / slope, -2*box_bound);
    } else {
      //We use y = mx + b and use x points as 2*box_bound and -2 * box_bound
      pt_rot1 = point_t(  2*box_bound,  slope*2*box_bound + intercept);
      pt_rot2 = point_t( -2*box_bound, -slope*2*box_bound + intercept);
    }
  }

  bg::append(line, pt_rot1);
  bg::append(line, pt_rot2);

  /* Find intersection */
  bg::intersection(line,det->rectangle,A);
  bg::unique(A);

  if (A.size()==0||A.size()==1){
    /* Do nothing */
  } else {
    if( theta==PETSC_PI/2){
      /* Only use first point in special cases because second point
         is grabbed from direction
         90 degrees - vertical line */
      Q.reserve((int)det->y.size());
      for (i=0; i<(int)det->y.size(); i++) {
        pt_Q = point_t(A[0].get<0>(),det->y[i]);
        Q.emplace_back(pt_Q);
      }
    } else if (theta==0 || theta==2*PETSC_PI) {
      /* 0, 360 - horizontal line */
      Q.reserve((int)det->x.size());
      for (i=0; i<(int)det->x.size(); i++) {
        pt_Q = point_t(det->x[i], A[0].get<1>());
        Q.emplace_back(pt_Q);
      }
    } else if (theta==PETSC_PI) {
      /* 180 - source, detector switched location */
      Q.reserve((int)det->x.size());
      for (i=(det->x.size()-1); i>=0; i--) {
        pt_Q = point_t(det->x[i], A[0].get<1>());
        Q.emplace_back(pt_Q);
      }
    } else if (theta==3*PETSC_PI/2) {
      /* 270 - source, detector switched location */
      Q.reserve((int)det->y.size());
      for (i=(det->y.size()-1); i>=0; i--) {
        pt_Q = point_t(A[0].get<0>(), det->y[i]);
        Q.emplace_back(pt_Q);
      }
    } else {
      Q.reserve((int)det->x.size() + (int)det->y.size());
      for (i=0;i<(int)det->x.size();i++) {
        _y = (A[1].get<1>() - A[0].get<1>())/(A[1].get<0>() - A[0].get<0>())*det->x[i]
          + (A[0].get<1>()*A[1].get<0>()- A[1].get<1>()*A[0].get<0>())/(A[1].get<0>() - A[0].get<0>());
        if ((_y > det->omega[2]) && (_y < det->omega[3])){
          pt_Q = point_t(det->x[i],_y);
          Q.emplace_back(pt_Q);
        }
      }
      for (i=0;i<(int)det->y.size();i++) {
        _x = (det->y[i] - (A[0].get<1>()*A[1].get<0>() - A[1].get<1>()*A[0].get<0>())/
            (A[1].get<0>() - A[0].get<0>()))/((A[1].get<1>() - A[0].get<1>())/(A[1].get<0>() - A[0].get<0>()));
        if ((_x > det->omega[0]) && (_x < det->omega[1])){
          pt_Q = point_t(_x,det->y[i]);
          Q.emplace_back(pt_Q);
        }
      }
    }

    if (pirt_g->debug) {
      ierr = PetscPrintf(PETSC_COMM_SELF,"tau_idx: %d, theta_idx: %d, theta: %e, det.x/y.size: %d & %d, det.m: %d & %d, A.size(): %d, Q.size(): %d\n",\
          tau_idx, theta_idx, theta, (PetscInt) det->x.size(), (PetscInt) det->y.size(), \
          (PetscInt) det->m[0], (PetscInt) det->m[1], (PetscInt) A.size(), (PetscInt) Q.size());CHKERRQ(ierr);
    }

    if (pirt_g->debug) {
      j = 0;
      size = Q.size();
      for (i=0; i<size; i++) {
        if(!bg::intersects(Q[j], det->rectangle)){
          PetscPrintf(PETSC_COMM_SELF, "warn");
          Q.erase(Q.begin() + j);
        } else {
          j++;
        }
      }
    }

    /* Sort for ascending x */
    std::sort(Q.begin(), Q.end(), point_comparison);
    *d_nz = 0; *o_nz = 0;

    /* Get distance between neighbors */
    for (i=0; i<(int)Q.size()-1; i++) {
      point_t tmp_point = point_t( (Q[i].get<0>() + Q[i+1].get<0>())/2,
          (Q[i].get<1>() + Q[i+1].get<1>())/2 );
      x_ind = ( tmp_point.get<0>() - det->omega[0] )/det->dz[0];
      y_ind = ( tmp_point.get<1>() - det->omega[2] )/det->dz[1];

      /* m[1] and m[0] are (confusingly) correct! */
      if (x_ind>=0 && x_ind<det->m[1] && y_ind>=0 && y_ind<det->m[0]) {
        j_mat = x_ind * det->m[0] + y_ind;

        if(j_mat<c_Iend && j_mat >= c_Istart){
          /* Diagonal part */
          *d_nz = *d_nz + 1;
        } else {
          *o_nz = *o_nz + 1;
        }
      }
    }

    /* TODO : Add Lvec unique here
       auto last = std::unique(index.begin(), index.end());
       index.erase(last,index.end()); */
  }

  PetscFunctionReturn(0);
}

