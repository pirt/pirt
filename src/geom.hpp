#ifndef GEOM_H
#define GEOM_H

#define BOOST_ALLOW_DEPRECATED_HEADERS
#include <boost/geometry.hpp>
#include <boost/geometry/geometries/point_xy.hpp>
#include <boost/geometry/geometries/polygon.hpp>
#include <boost/geometry/geometries/linestring.hpp>
#undef  BOOST_ALLOW_DEPRECATED_HEADERS

#include "pirtctx.hpp"

namespace bg = boost::geometry;
typedef bg::model::point<double, 2, bg::cs::cartesian> point_t;
typedef bg::model::polygon<point_t> polygon_t;
typedef bg::model::segment<point_t> segment_t;
typedef bg::model::linestring<point_t> linestring_t;

typedef struct {

  int     numThetan,ntau;
  double  omega[4], m[2], dz[2], Tol;

  std::vector<double>   x,y; /* make this point_t ? */
  std::vector<point_t>  DetKnot0,SourceKnot0;

  polygon_t  rectangle;

} detctx;

std::vector<double> linspace(double a, double b, size_t N);

PetscErrorCode init_detctx(detctx *det, pirt_global_ctx *pirt_g);
PetscErrorCode setMatrixElements(PetscInt tau_idx, PetscInt theta_idx, detctx *det, pirt_global_ctx *pirt_g, pirt_local_ctx *pirt_l);
PetscErrorCode countMatrixElements(PetscInt tau_idx, PetscInt theta_idx, detctx *det, pirt_global_ctx *pirt_g, pirt_local_ctx *pirt_l, PetscInt *d_nz, PetscInt *o_nz);

#endif
