/*! \file io.cxx
 * I/O routines to load primary and auxiallary data and store results to files.
 */

#include "io.hpp"

/* --------------------------------------------------------------------- */
/*!
  Create and set parallel distribution of matrix & vectors

  \param *pirt_g  - pirt global context
  \param *pirt_l  - pirt local context
  \return ierr    - PetscErrorCode

*/
PetscErrorCode init_layouts(pirt_global_ctx  *pirt_g, pirt_local_ctx   *pirt_l){

  PetscErrorCode ierr;
  PetscFunctionBeginUser;

  /* Because of the way we use FFTW, we don't want to have a division
     between one of the arrays we are transforming. As such, we break up
     the matrix on only numThetan, keeping the groups of ntau together. */
  pirt_l->ntheta_local = PETSC_DECIDE;
  ierr = PetscSplitOwnership(pirt_l->tasksubcomm, &(pirt_l->ntheta_local), &(pirt_l->ntheta));CHKERRQ(ierr);
  pirt_l->ntau_local = PETSC_DECIDE;
  ierr = PetscSplitOwnership(pirt_l->tasksubcomm, &(pirt_l->ntau_local), &(pirt_l->ntau));CHKERRQ(ierr);

  /* Create A matrix */
  ierr = MatCreate(pirt_l->tasksubcomm, &(pirt_l->A));CHKERRQ(ierr);
  ierr = MatSetType(pirt_l->A, MATMPIAIJ);CHKERRQ(ierr);

  /* Create vector to hold reconstructed sample */
  ierr = VecCreate(pirt_l->tasksubcomm, &(pirt_l->recon_sample));CHKERRQ(ierr);

  /* Create exptheta and shifts to hold angles and reconstucted shifts */
  ierr = VecCreate(pirt_l->tasksubcomm, &(pirt_l->exptheta));CHKERRQ(ierr);
  ierr = VecSetSizes(pirt_l->exptheta, pirt_l->ntheta_local, pirt_l->ntheta);CHKERRQ(ierr);
  ierr = VecSetUp(pirt_l->exptheta);CHKERRQ(ierr);

  ierr = VecCreate(pirt_l->tasksubcomm, &(pirt_l->shifts));CHKERRQ(ierr);
  ierr = VecSetSizes(pirt_l->shifts, pirt_l->ntheta_local, pirt_l->ntheta);CHKERRQ(ierr);
  ierr = VecSetUp(pirt_l->shifts);CHKERRQ(ierr);

  /* Create global exptheta and shifts for I/O and synchronization respectively */
  ierr = VecCreate(PETSC_COMM_WORLD, &(pirt_g->shifts));CHKERRQ(ierr);
  ierr = VecCreate(PETSC_COMM_WORLD, &(pirt_g->exptheta));CHKERRQ(ierr);
  ierr = VecSetSizes(pirt_g->shifts,   PETSC_DECIDE, pirt_g->ntheta);CHKERRQ(ierr);
  ierr = VecSetSizes(pirt_g->exptheta, PETSC_DECIDE, pirt_g->ntheta);CHKERRQ(ierr);
  ierr = VecSetUp(pirt_g->shifts);CHKERRQ(ierr);
  ierr = VecSetUp(pirt_g->exptheta);CHKERRQ(ierr);
  ierr = PetscObjectSetName((PetscObject)pirt_g->exptheta,"angles");CHKERRQ(ierr);
  ierr = PetscObjectSetName((PetscObject)pirt_l->exptheta,"angles");CHKERRQ(ierr);
  /* To prevent early termination of outer sweeps when not using combie mean/median */
  ierr = VecSet(pirt_g->shifts, 1.0);CHKERRQ(ierr);

  /* Setup local and global dimensions for tomography projection matrix */
  ierr = MatSetSizes(pirt_l->A, pirt_l->ntheta_local*pirt_l->ntau, pirt_l->ntau_local*pirt_l->ntau, \
      pirt_l->ntau*pirt_l->ntheta, pirt_l->ntau*pirt_l->ntau);CHKERRQ(ierr);

  ierr = VecSetType(pirt_l->recon_sample, VECMPI);CHKERRQ(ierr);
  if (pirt_g->joint_ec) {
    ierr = VecSetSizes(pirt_l->recon_sample, pirt_l->ntheta_local + (pirt_l->ntau_local*pirt_l->ntau), PETSC_DECIDE);CHKERRQ(ierr);
  } else {
    ierr = VecSetSizes(pirt_l->recon_sample, pirt_l->ntau_local*pirt_l->ntau, PETSC_DECIDE);CHKERRQ(ierr);
  }
  ierr = VecSetUp(pirt_l->recon_sample);CHKERRQ(ierr);

  /* 'fake' preallocation, real preallocation will be done latter */
  ierr = MatMPIAIJSetPreallocation(pirt_l->A,10,NULL,10,NULL);CHKERRQ(ierr);
  ierr = MatSetUp(pirt_l->A);CHKERRQ(ierr);

  /* create vector to hold experimental data, if using synthetic data, name the vector dataset for HDF5 I/O */
  ierr = MatCreateVecs(pirt_l->A, NULL, &(pirt_l->expdata));CHKERRQ(ierr);
  if (!pirt_g->synthetic_data){
    ierr = PetscObjectSetName((PetscObject)pirt_l->expdata,"sinogram");CHKERRQ(ierr);
  }
  /* work vector to hold smoothed data */
  ierr = VecDuplicate(pirt_l->expdata, &(pirt_l->workvec));  CHKERRQ(ierr);

  PetscFunctionReturn(ierr);
}

/* --------------------------------------------------------------------- */
/*!
  Load angles into exptheta.

  \note The angles are loaded into a global vector (in pirt_g) and then
  "scattered" into all local vectors (in pirt_l).

  \param filename - filename of hdf5 file containg the angles
  \param *pirt_g  - pirt global context
  \return ierr    - PetscErrorCode

*/
PetscErrorCode load_angles(char filename[PETSC_MAX_PATH_LEN],
    pirt_global_ctx  *pirt_g) {

  PetscViewer    hdf5_viewer;
  PetscErrorCode ierr;

  PetscFunctionBeginUser;

  ierr = PetscViewerHDF5Open(PETSC_COMM_WORLD,filename,
      FILE_MODE_READ,&hdf5_viewer);CHKERRQ(ierr);
  ierr = PetscViewerHDF5PushGroup(hdf5_viewer, "/theta");CHKERRQ(ierr);
  ierr = VecLoad(pirt_g->exptheta,hdf5_viewer);CHKERRQ(ierr);
  ierr = PetscViewerDestroy(&hdf5_viewer);CHKERRQ(ierr);

  ierr = VecScatterBegin(pirt_g->vscat_global_to_tasks,
      pirt_g->exptheta, pirt_g->gexptheta,
      INSERT_VALUES, SCATTER_FORWARD);CHKERRQ(ierr);
  ierr = VecScatterEnd(pirt_g->vscat_global_to_tasks,
      pirt_g->exptheta, pirt_g->gexptheta,
      INSERT_VALUES, SCATTER_FORWARD);CHKERRQ(ierr);

  PetscFunctionReturn(ierr);
}

/* --------------------------------------------------------------------- */
/*!
  Load sinogram.

  \param filename - filename of hdf5 file
  \param *pirt_l  - pirt local context
  \return ierr    - PetscErrorCode

*/
PetscErrorCode load_data(char filename[PETSC_MAX_PATH_LEN],
    pirt_local_ctx   *pirt_l){

  PetscViewer    hdf5_viewer;
  char           appendchars[PETSC_MAX_PATH_LEN];
  char           sinogroup[PETSC_MAX_PATH_LEN];
  PetscErrorCode ierr;

  PetscFunctionBeginUser;

  sprintf(sinogroup,"sino");
  sprintf(appendchars,"%d",(int)(pirt_l->curr_task_slice));
  strcat(sinogroup, appendchars);

  ierr = PetscViewerHDF5Open(pirt_l->tasksubcomm, filename, FILE_MODE_READ, &hdf5_viewer);CHKERRQ(ierr);
  ierr = PetscViewerHDF5PushGroup(hdf5_viewer, sinogroup);CHKERRQ(ierr);
  ierr = VecLoad(pirt_l->expdata,hdf5_viewer);CHKERRQ(ierr);
  ierr = PetscViewerHDF5PopGroup(hdf5_viewer);CHKERRQ(ierr);
  ierr = PetscViewerDestroy(&hdf5_viewer);CHKERRQ(ierr);

  PetscFunctionReturn(ierr);
}

/* --------------------------------------------------------------------- */
/*!
  Load the solution which will be used to generate sinogram when working
  with synthetic data.

  \param filename - filename of hdf5 file
  \param *pirt_g  - pirt global context
  \param *pirt_l  - pirt local context
  \return ierr    - PetscErrorCode

*/
PetscErrorCode load_synthetic(char filename[PETSC_MAX_PATH_LEN],
    pirt_global_ctx  *pirt_g,
    pirt_local_ctx  *pirt_l){

  PetscErrorCode ierr;
  PetscViewer    hdf5_viewer;
  Vec            loadvec;

  PetscFunctionBeginUser;

  ierr = PetscViewerHDF5Open(pirt_l->tasksubcomm, filename, FILE_MODE_READ, &hdf5_viewer);CHKERRQ(ierr);

  ierr = PetscViewerHDF5PushGroup(hdf5_viewer, "/sample");CHKERRQ(ierr);
  ierr = PetscObjectSetName((PetscObject)pirt_l->recon_sample,"input");CHKERRQ(ierr);

  PetscPrintf(PETSC_COMM_WORLD,"ntau is %d: \n",pirt_g->ntau);

  if (pirt_g->joint_ec) {
    ierr = VecGetSubVector(pirt_l->recon_sample, pirt_l->joint_sample_is, &loadvec);CHKERRQ(ierr);
    ierr = PetscObjectSetName((PetscObject)loadvec,"input");CHKERRQ(ierr);
    ierr = VecLoad(loadvec, hdf5_viewer);CHKERRQ(ierr);
    ierr = VecRestoreSubVector(pirt_l->recon_sample, pirt_l->joint_sample_is, &loadvec);CHKERRQ(ierr);
  } else {
    ierr = VecLoad(pirt_l->recon_sample, hdf5_viewer);CHKERRQ(ierr);
  }
  ierr = PetscViewerHDF5PopGroup(hdf5_viewer);CHKERRQ(ierr);

  ierr = PetscViewerPushFormat(PETSC_VIEWER_STDOUT_WORLD,PETSC_VIEWER_ASCII_INFO);CHKERRQ(ierr);
  ierr = VecView(pirt_l->expdata,PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr);
  ierr = PetscViewerPopFormat(PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr);

  PetscFunctionReturn(ierr);
}

/* --------------------------------------------------------------------- */
/*!
  Load exact shifts, for sanity checks

  \param filename - filename of hdf5 file
  \param *pirt_g  - pirt global context
  \param *pirt_l  - pirt local context
  \return ierr    - PetscErrorCode

*/
PetscErrorCode load_exact_shifts(char filename[PETSC_MAX_PATH_LEN],
    pirt_global_ctx *pirt_g,
    pirt_local_ctx  *pirt_l){

  PetscErrorCode ierr;
  PetscViewer    hdf5_shifts_viewer;
  Vec            shiftvec;

  PetscFunctionBeginUser;

  ierr = PetscViewerHDF5Open(pirt_l->tasksubcomm, filename, FILE_MODE_READ,&hdf5_shifts_viewer);CHKERRQ(ierr);

  if (pirt_g->alt_ec) {
    ierr = PetscObjectSetName((PetscObject)pirt_l->shifts,"shifts");CHKERRQ(ierr);
    ierr = VecLoad(pirt_l->shifts,hdf5_shifts_viewer);CHKERRQ(ierr);
  }

  if (pirt_g->joint_ec) {
    ierr = VecGetSubVector(pirt_l->recon_sample, pirt_l->joint_shifts_is, &shiftvec);CHKERRQ(ierr);
    ierr = PetscObjectSetName((PetscObject)shiftvec,"shifts");CHKERRQ(ierr);
    ierr = VecLoad(shiftvec,hdf5_shifts_viewer);CHKERRQ(ierr);
    ierr = VecRestoreSubVector(pirt_l->recon_sample, pirt_l->joint_shifts_is, &shiftvec);CHKERRQ(ierr);
  }

  ierr = PetscViewerDestroy(&hdf5_shifts_viewer);CHKERRQ(ierr);
  PetscFunctionReturn(ierr);
}


/* --------------------------------------------------------------------- */
/*!
  Save reconstructed sample (and shifts if applicable) to hdf5 files

  \param *pirt_l  - pirt local context
  \return ierr    - PetscErrorCode

*/
PetscErrorCode save_solution(pirt_local_ctx *pirt_l){

  PetscViewer    hdf5_viewer;     /* viewer to write reconstructed sample hdf5 */
  Vec            solvec;
  Vec            shiftvec;
  PetscErrorCode ierr;
  char           solfilename[PETSC_MAX_PATH_LEN];

  PetscFunctionBeginUser;
  sprintf(solfilename,"sol_%d.h5",(int)(pirt_l->curr_task_slice));
  ierr = PetscViewerHDF5Open(pirt_l->tasksubcomm, solfilename, FILE_MODE_WRITE, &hdf5_viewer);CHKERRQ(ierr);

  if (pirt_l->joint_ec) {
    ierr = PetscViewerHDF5PushGroup(hdf5_viewer, "/reconstruction");CHKERRQ(ierr);
    ierr = VecGetSubVector(pirt_l->recon_sample, pirt_l->joint_sample_is, &solvec);CHKERRQ(ierr);
    ierr = PetscObjectSetName((PetscObject)solvec,"solution");CHKERRQ(ierr);
    ierr = VecView(solvec, hdf5_viewer);CHKERRQ(ierr);
    ierr = VecRestoreSubVector(pirt_l->recon_sample, pirt_l->joint_sample_is, &solvec);CHKERRQ(ierr);

    ierr = PetscViewerHDF5PushGroup(hdf5_viewer, "/cor");CHKERRQ(ierr);
    ierr = VecGetSubVector(pirt_l->recon_sample, pirt_l->joint_shifts_is, &shiftvec);CHKERRQ(ierr);
    ierr = PetscObjectSetName((PetscObject)shiftvec,"shifts");CHKERRQ(ierr);
    ierr = VecView(shiftvec, hdf5_viewer);CHKERRQ(ierr);
    ierr = VecRestoreSubVector(pirt_l->recon_sample, pirt_l->joint_shifts_is, &shiftvec);CHKERRQ(ierr);

    ierr = PetscObjectSetName((PetscObject)pirt_l->smoothed_data,"sinogram");CHKERRQ(ierr);
    ierr = VecView(pirt_l->smoothed_data, hdf5_viewer);CHKERRQ(ierr);

    ierr = PetscViewerHDF5PopGroup(hdf5_viewer);CHKERRQ(ierr);
    ierr = PetscViewerHDF5PopGroup(hdf5_viewer);CHKERRQ(ierr);
    ierr = PetscViewerDestroy(&hdf5_viewer);CHKERRQ(ierr);

  } else if (pirt_l->alt_ec) {

    ierr = PetscViewerHDF5PushGroup(hdf5_viewer, "/reconstruction");CHKERRQ(ierr);
    ierr = PetscObjectSetName((PetscObject)pirt_l->recon_sample,"solution");CHKERRQ(ierr);
    ierr = VecView(pirt_l->recon_sample, hdf5_viewer);CHKERRQ(ierr);

    ierr = PetscViewerHDF5PushGroup(hdf5_viewer, "/cor");CHKERRQ(ierr);
    ierr = PetscObjectSetName((PetscObject)pirt_l->shifts,"shifts");CHKERRQ(ierr);
    ierr = VecView(pirt_l->shifts, hdf5_viewer);CHKERRQ(ierr);

    ierr = PetscObjectSetName((PetscObject)pirt_l->smoothed_data,"sinogram");CHKERRQ(ierr);
    ierr = VecView(pirt_l->smoothed_data, hdf5_viewer);CHKERRQ(ierr);

    ierr = PetscViewerHDF5PopGroup(hdf5_viewer);CHKERRQ(ierr);
    ierr = PetscViewerHDF5PopGroup(hdf5_viewer);CHKERRQ(ierr);
    ierr = PetscViewerDestroy(&hdf5_viewer);CHKERRQ(ierr);

  } else {

    ierr = PetscViewerHDF5PushGroup(hdf5_viewer, "/reconstruction");CHKERRQ(ierr);
    ierr = PetscObjectSetName((PetscObject)pirt_l->recon_sample,"solution");CHKERRQ(ierr);
    ierr = VecView(pirt_l->recon_sample, hdf5_viewer);CHKERRQ(ierr);

    ierr = PetscViewerHDF5PopGroup(hdf5_viewer);CHKERRQ(ierr);
    ierr = PetscViewerDestroy(&hdf5_viewer);CHKERRQ(ierr);
  }
  PetscFunctionReturn(ierr);
}

/* --------------------------------------------------------------------- */
/*!
  Load reconstructed sample (and shifts if applicable) from
  checkpointed hdf5 files

  \param *pirt_l  - pirt local context
  \return ierr    - PetscErrorCode

*/
PetscErrorCode load_solution(pirt_local_ctx *pirt_l){

  PetscViewer    hdf5_viewer;     /* viewer to write reconstructed sample hdf5 */
  Vec            solvec;
  PetscErrorCode ierr;
  char           solfilename[PETSC_MAX_PATH_LEN];

  PetscFunctionBeginUser;

  sprintf(solfilename,"sol_%d.h5",(int)(pirt_l->curr_task_slice));
  ierr = PetscViewerHDF5Open(pirt_l->tasksubcomm, solfilename, FILE_MODE_READ, &hdf5_viewer);CHKERRQ(ierr);

  if (pirt_l->joint_ec) {
    ierr = PetscViewerHDF5PushGroup(hdf5_viewer, "/reconstruction");CHKERRQ(ierr);
    ierr = VecGetSubVector(pirt_l->recon_sample, pirt_l->joint_sample_is, &solvec);CHKERRQ(ierr);
    ierr = PetscObjectSetName((PetscObject)solvec,"solution");CHKERRQ(ierr);
    ierr = VecLoad(solvec, hdf5_viewer);CHKERRQ(ierr);
    ierr = VecRestoreSubVector(pirt_l->recon_sample, pirt_l->joint_sample_is, &solvec);CHKERRQ(ierr);

    ierr = PetscViewerHDF5PushGroup(hdf5_viewer, "/cor");CHKERRQ(ierr);
    ierr = VecLoad(pirt_l->expdata, hdf5_viewer);CHKERRQ(ierr);

    ierr = PetscViewerHDF5PopGroup(hdf5_viewer);CHKERRQ(ierr);
    ierr = PetscViewerHDF5PopGroup(hdf5_viewer);CHKERRQ(ierr);
    ierr = PetscViewerDestroy(&hdf5_viewer);CHKERRQ(ierr);

  } else if (pirt_l->alt_ec) {

    ierr = PetscViewerHDF5PushGroup(hdf5_viewer, "/reconstruction");CHKERRQ(ierr);
    ierr = VecLoad(pirt_l->recon_sample, hdf5_viewer);CHKERRQ(ierr);

    ierr = PetscViewerHDF5PushGroup(hdf5_viewer, "/cor");CHKERRQ(ierr);
    ierr = VecLoad(pirt_l->expdata, hdf5_viewer);CHKERRQ(ierr);

    ierr = PetscViewerHDF5PopGroup(hdf5_viewer);CHKERRQ(ierr);
    ierr = PetscViewerHDF5PopGroup(hdf5_viewer);CHKERRQ(ierr);
    ierr = PetscViewerDestroy(&hdf5_viewer);CHKERRQ(ierr);

  } else {

    ierr = PetscViewerHDF5PushGroup(hdf5_viewer, "/reconstruction");CHKERRQ(ierr);
    ierr = PetscObjectSetName((PetscObject)pirt_l->recon_sample,"solution");CHKERRQ(ierr);
    ierr = VecLoad(pirt_l->recon_sample, hdf5_viewer);CHKERRQ(ierr);

    ierr = PetscViewerHDF5PushGroup(hdf5_viewer, "/cor");CHKERRQ(ierr);
    ierr = VecLoad(pirt_l->expdata, hdf5_viewer);CHKERRQ(ierr);

    ierr = PetscViewerHDF5PopGroup(hdf5_viewer);CHKERRQ(ierr);
    ierr = PetscViewerHDF5PopGroup(hdf5_viewer);CHKERRQ(ierr);
    ierr = PetscViewerDestroy(&hdf5_viewer);CHKERRQ(ierr);
  }
  PetscFunctionReturn(ierr);
}


