#ifndef IO_H
#define IO_H

#include <petscviewerhdf5.h>
#include "pirtctx.hpp"

PetscErrorCode init_layouts(pirt_global_ctx *pirt_g, pirt_local_ctx *pirt_l);

PetscErrorCode load_data(char filename[PETSC_MAX_PATH_LEN], pirt_local_ctx *pirt_l);
PetscErrorCode load_angles(char filename[PETSC_MAX_PATH_LEN],pirt_global_ctx *pirt_g);

PetscErrorCode load_synthetic(char filename[PETSC_MAX_PATH_LEN], pirt_global_ctx *pirt_g, pirt_local_ctx *pirt_l);
PetscErrorCode load_exact_shifts(char filename[PETSC_MAX_PATH_LEN], pirt_global_ctx *pirt_g, pirt_local_ctx *pirt_l);

PetscErrorCode save_solution(pirt_local_ctx *pirt_l);
PetscErrorCode load_solution(pirt_local_ctx *pirt_l);

#endif
