#include <petsctao.h>
#include "matrix.hpp"
#include "utils.hpp"

int main(int argc,char **args)
{

  pirt_global_ctx   pirt_g;        /* global pirt context, holds global data structures */
  pirt_local_ctx    pirt_l;        /* local pirt context, holds task data */
  detctx            det;           /* detector geometry information context */
  const PetscInt    *local_slices; /* local slices for task comm */
  PetscInt          i;
  PetscErrorCode    ierr;          /* to display error strings, if any fro petsc routines */

  ierr = initialize_pirt(argc, args, &pirt_g, &pirt_l, &det);
  ierr = PetscPrintf(PETSC_COMM_WORLD,"Initialized PIRT\n");CHKERRQ(ierr);

  if (pirt_l.task_rank==0) {
    ierr = ISGetIndices(pirt_g.org_is, &local_slices);CHKERRQ(ierr);
  }

  PetscLogStagePush(pirt_g.stages[0]);
  ierr = construct_matrix(&pirt_g, &pirt_l, &det);CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD,"Constructed Matrix\n");CHKERRQ(ierr);
  PetscLogStagePop();

  PetscViewer viewer;
  ierr = PetscPrintf(pirt_l.tasksubcomm, "Write matrix into file A.bin ...\n");CHKERRQ(ierr);
  ierr = PetscViewerBinaryOpen(pirt_l.tasksubcomm, "A.bin", FILE_MODE_WRITE, &viewer);CHKERRQ(ierr);
  ierr = MatView(pirt_l.A, viewer);CHKERRQ(ierr);
  ierr = PetscViewerDestroy(&viewer);CHKERRQ(ierr);
  ierr = PetscViewerPushFormat(PETSC_VIEWER_STDOUT_(pirt_l.tasksubcomm), PETSC_VIEWER_ASCII_INFO);CHKERRQ(ierr);
  ierr = MatView(pirt_l.A, PETSC_VIEWER_STDOUT_(pirt_l.tasksubcomm));CHKERRQ(ierr);
  ierr = PetscViewerPopFormat(PETSC_VIEWER_STDOUT_(pirt_l.tasksubcomm));CHKERRQ(ierr);

  ierr = finalize_pirt(&pirt_g, &pirt_l);CHKERRQ(ierr);

  ierr = PetscFinalize();
  return ierr;
}
