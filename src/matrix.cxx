/*! \file matrix.cxx
 * Routines to create tomography projection matrix.
 */

#include "matrix.hpp"

/* --------------------------------------------------------------------- */
/*!
  Construct the tomography projection matrix

  \param *pirt_g  - pirt global context
  \param *pirt_l  - pirt local context
  \param *det     - detector geometry context
  \return ierr    - PetscErrorCode

*/
PetscErrorCode construct_matrix(pirt_global_ctx *pirt_g, pirt_local_ctx *pirt_l, detctx *det){
  Vec            rowmax;
  PetscInt       *rowmaxloc;
  PetscReal      matmax;
  PetscInt       Istart, Iend;
  PetscInt       c_Istart, c_Iend;
  PetscInt       tau_idx, theta_idx;
  PetscInt       i,j=0;
  PetscInt       *o_nz_a, *d_nz_a;
  PetscInt       o_nz, d_nz;
  PetscInt       debug_nz;
  PetscBool      matflg;
  char           matfile[PETSC_MAX_PATH_LEN];
  PetscViewer    matviewer;
  PetscErrorCode ierr;

  PetscFunctionBeginUser;

  ierr = PetscOptionsGetString(NULL,NULL, "-matfile",
      matfile, PETSC_MAX_PATH_LEN, &matflg);CHKERRQ(ierr);

  if (matflg) {
    ierr = PetscPrintf(pirt_l->tasksubcomm, "Reading in tomography projection matrix from ...\n");CHKERRQ(ierr);
    ierr = PetscViewerBinaryOpen(pirt_l->tasksubcomm, matfile, FILE_MODE_READ, &matviewer);CHKERRQ(ierr);
    ierr = MatLoad(pirt_l->A, matviewer); CHKERRQ(ierr);
    ierr = PetscViewerDestroy(&matviewer);CHKERRQ(ierr);
  } else {

    /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
       Set options for A matrix, get row and diagonal column ownership ranges
       - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
    ierr = MatSetOption(pirt_l->A, MAT_IGNORE_OFF_PROC_ENTRIES, PETSC_TRUE);CHKERRQ(ierr);
    ierr = MatSetOption(pirt_l->A, MAT_NEW_NONZERO_LOCATION_ERR, PETSC_TRUE);CHKERRQ(ierr);
    ierr = MatGetOwnershipRange(pirt_l->A, &Istart, &Iend);CHKERRQ(ierr);
    ierr = MatGetOwnershipRangeColumn(pirt_l->A, &c_Istart, &c_Iend);CHKERRQ(ierr);
    ierr = PetscMalloc1(Iend-Istart, &d_nz_a);CHKERRQ(ierr);
    ierr = PetscMalloc1(Iend-Istart, &o_nz_a);CHKERRQ(ierr);

    /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
       Loop over all rows and preallocate
       - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
    for(i=Istart; i<Iend; i++) {

      theta_idx = i/det->ntau;
      tau_idx   = i%det->ntau;
      ierr = countMatrixElements(tau_idx, theta_idx, det, pirt_g, pirt_l, &d_nz, &o_nz);CHKERRQ(ierr);

      /* Handle entries in diagonal part of matrix, need one on diagonal no matter what */
      if (d_nz>1) {
        d_nz_a[j] = d_nz;
      } else {
        d_nz_a[j] = 1;
      }

      /* Bounds check in debug mode */
      if (pirt_g->debug) {
        if (d_nz > (c_Iend - c_Istart)) {
          debug_nz  =  d_nz_a[j];
          d_nz_a[j] = c_Iend - c_Istart;
          ierr = PetscPrintf(PETSC_COMM_SELF,"Warning, erronous estimation! i: %d, tau_idx: %d, theta_idx: %d, nnz : %d, max_nnz : %d \n",
              i,tau_idx, theta_idx, debug_nz, (c_Iend - c_Istart));CHKERRQ(ierr);
        }
      }

      /* Handle off diagonal part of matrix */
      o_nz_a[j] = o_nz;
      j = j + 1;

      /* Dump preallocation calls to screen in debug mode */
      if (pirt_g->debug) {
        ierr = PetscPrintf(PETSC_COMM_SELF,"i: %d, tau_idx: %d, theta_idx: %d, d_nz_a: %d, o_nz:a: %d\n",
            i,tau_idx, theta_idx, d_nz_a, o_nz_a);CHKERRQ(ierr);CHKERRQ(ierr);
      }
    }

    /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
       Ensure all MPI ranks have valid arguments, perform preallocation
       - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
    MPI_Barrier(pirt_l->tasksubcomm);
    /* Note : Istart is ignored */
    ierr = MatMPIAIJSetPreallocation(pirt_l->A, Istart, d_nz_a, Istart, o_nz_a);CHKERRQ(ierr);


    /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
       Loop over all rows and set matrix values
       - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
    for(i=Istart;i<Iend;i++) {

      theta_idx = i/det->ntau;
      tau_idx   = i%det->ntau;
      ierr = setMatrixElements(tau_idx, theta_idx, det, pirt_g, pirt_l);CHKERRQ(ierr);
    }

    /* Assembly must be called to get the parallel version of the matrix correctly distributed! */
    ierr = MatAssemblyBegin(pirt_l->A, MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
    ierr = MatAssemblyEnd(pirt_l->A, MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
    MPI_Barrier(pirt_l->tasksubcomm);

    ierr = PetscFree(d_nz_a);CHKERRQ(ierr);
    ierr = PetscFree(o_nz_a);CHKERRQ(ierr);

  }

  if (pirt_g->debug){
    ierr = PetscViewerPushFormat(PETSC_VIEWER_STDOUT_(pirt_l->tasksubcomm), PETSC_VIEWER_ASCII_INFO);CHKERRQ(ierr);
    ierr = MatView(pirt_l->A, PETSC_VIEWER_STDOUT_(pirt_l->tasksubcomm));CHKERRQ(ierr);
    ierr = PetscViewerPopFormat(PETSC_VIEWER_STDOUT_(pirt_l->tasksubcomm));CHKERRQ(ierr);
  }

  if (pirt_g->normalize_mat) {
    ierr = PetscMalloc1(pirt_l->ntheta_local*pirt_l->ntau, &rowmaxloc);CHKERRQ(ierr);
    ierr = VecCreate(pirt_l->tasksubcomm, &rowmax);CHKERRQ(ierr);
    ierr = VecSetType(rowmax, VECSTANDARD);CHKERRQ(ierr);
    ierr = VecSetSizes(rowmax, pirt_l->ntheta_local*pirt_l->ntau, PETSC_DECIDE);CHKERRQ(ierr);
    ierr = MatGetRowMax(pirt_l->A, rowmax, rowmaxloc);CHKERRQ(ierr);
    ierr = VecMax(rowmax, NULL, &matmax);CHKERRQ(ierr);
    ierr = MatScale(pirt_l->A, (1/PetscSqrtReal(matmax)));CHKERRQ(ierr);
    ierr = VecDestroy(&rowmax);CHKERRQ(ierr);
    ierr = PetscFree(rowmaxloc);CHKERRQ(ierr);
  }

  PetscFunctionReturn(0);
}
