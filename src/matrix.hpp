#ifndef MATRIX_H
#define MATRIX_H

#include "geom.hpp"

PetscErrorCode construct_matrix(pirt_global_ctx *pirt_g, pirt_local_ctx *pirt_l, detctx *det);

#endif
