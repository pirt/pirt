#include <petsctao.h>
#include "utils.hpp"
#include "matrix.hpp"
#include "solver.hpp"
#include "solver_alt.hpp"
#include "solver_joint.hpp"

int main(int argc,char **args)
{
  pirt_global_ctx   pirt_g;             /* global pirt context, holds global data structures */
  pirt_local_ctx    pirt_l;             /* local pirt context, holds task data */
  detctx            det;                /* detector geometry information context */
  const PetscInt    *local_slices;      /* local slices for task comm */
  PetscInt          i,j;                /* iteration indices */
  Vec               sample_vec;         /* sample vector, used for joint reconstruction for synthetic data */
  PetscErrorCode    ierr;               /* to display error strings, if any fro petsc routines */

  ierr = initialize_pirt(argc, args, &pirt_g, &pirt_l, &det);
  ierr = PetscPrintf(PETSC_COMM_WORLD,"Initialized PIRT\n");CHKERRQ(ierr);

  if (pirt_l.task_rank==0) {
    ierr = ISGetIndices(pirt_g.org_is, &local_slices);CHKERRQ(ierr);
  }

  PetscLogStagePush(pirt_g.stages[0]);
  ierr = construct_matrix(&pirt_g, &pirt_l, &det);CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD, "Constructed Matrix\n");CHKERRQ(ierr);
  PetscLogStagePop();

  /* Load data from h5 files & generate the 'data' by doing a MatMult */
  if (pirt_g.synthetic_data) {
    ierr = load_synthetic(pirt_g.sinofile, &pirt_g, &pirt_l);CHKERRQ(ierr);
    /* If using joint reconstruction algorithm, this requires extracting the
     * sample vector from the reconstruction vector. */
    if (pirt_g.joint_ec) {
      ierr = VecGetSubVector(pirt_l.recon_sample, pirt_l.joint_sample_is, &sample_vec);CHKERRQ(ierr);
      ierr = MatMult(pirt_l.A, sample_vec, pirt_l.expdata);CHKERRQ(ierr);
      ierr = VecRestoreSubVector(pirt_l.recon_sample, pirt_l.joint_sample_is, &sample_vec);CHKERRQ(ierr);
    } else {
      ierr = MatMult(pirt_l.A, pirt_l.recon_sample, pirt_l.expdata);CHKERRQ(ierr);
    }
    ierr = PetscPrintf(PETSC_COMM_WORLD, "Before solve...\n");CHKERRQ(ierr);
  }

  PetscLogStagePush(pirt_g.stages[1]);
  if (pirt_g.alt_ec) {
    ierr = solve_alternating_setup(&pirt_g, &pirt_l);CHKERRQ(ierr);
  } else if (pirt_g.joint_ec) {
    ierr = solve_joint_setup(&pirt_g, &pirt_l);CHKERRQ(ierr);
  } else {
    ierr = solve_setup(&pirt_g, &pirt_l);CHKERRQ(ierr);
  }
  ierr = PetscBarrier(NULL);CHKERRQ(ierr);


  /* Loop oer all slices on this task sub-comm */
  for (j=0; j<pirt_l.total_task_slices; j++) {
    if (pirt_l.task_rank==0) {
      pirt_l.curr_task_slice = local_slices[j];
    }
    ierr = MPI_Bcast(&pirt_l.curr_task_slice, 1, MPIU_INT, 0, pirt_l.tasksubcomm);CHKERRQ(ierr);
    ierr = PetscPrintf(pirt_l.tasksubcomm, "\ntaskcommid : %d, working on slice : %d\n", pirt_l.tasksubcommid, pirt_l.curr_task_slice);CHKERRQ(ierr);

    /* sweeps for this slice */
    for (i=0; i< pirt_g.overall_sweeps; i++) {

      if (pirt_l.regularize) {
        pirt_l.reg_weight = 5e-3 * (1 - 0.99 * ( (1.0*i)/(1.0*pirt_g.overall_sweeps)));
        if (pirt_l.alt_ec) {
          ierr = TaoBRGNSetRegularizerWeight(pirt_l.tao, pirt_l.reg_weight);CHKERRQ(ierr);
        }
      }
      ierr = PetscPrintf(PETSC_COMM_WORLD, "\n overall sweep all slices, number : %d, reg_weight is : %e\n", (int)(i), pirt_l.reg_weight);CHKERRQ(ierr);

      /* reset lcoal shifts for new batch */
      ierr = VecSet(pirt_l.shifts, 0.0);CHKERRQ(ierr);

      if (!pirt_g.synthetic_data && i==0) {
        ierr = load_data(pirt_g.sinofile, &pirt_l);CHKERRQ(ierr);
      }

      if (pirt_l.alt_ec || pirt_l.joint_ec) {
        if (i==0) {
          ierr = VecCopy(pirt_l.expdata, pirt_l.smoothed_data);CHKERRQ(ierr);
        } else {
          ierr = VecCopy(pirt_l.smoothed_data, pirt_l.expdata);CHKERRQ(ierr);
        }
      }

      if (pirt_g.alt_ec) {
        ierr = solve_alternating_execute(&pirt_g, &pirt_l);CHKERRQ(ierr);
      } else if (pirt_g.joint_ec) {
        ierr = solve_joint_execute(&pirt_g, &pirt_l);CHKERRQ(ierr);
      } else {
        ierr = solve_execute(&pirt_g, &pirt_l);CHKERRQ(ierr);
      }
    }

    /* Save solution after all sweeps */
    ierr = save_solution(&pirt_l);CHKERRQ(ierr);
    ierr = PetscBarrier(NULL);CHKERRQ(ierr);
  }


  if (pirt_g.alt_ec) {
    ierr = solve_alternating_destroy(&pirt_g, &pirt_l);CHKERRQ(ierr);
  } else if (pirt_g.joint_ec) {
    ierr = solve_joint_destroy(&pirt_g, &pirt_l);CHKERRQ(ierr);
  } else {
    ierr = solve_destroy(&pirt_g, &pirt_l);CHKERRQ(ierr);
  }
  PetscLogStagePop();

  if (pirt_l.task_rank==0) {
    ierr = ISRestoreIndices(pirt_g.org_is, &local_slices);CHKERRQ(ierr);
  }


  /* Cleanup & exit */
  ierr = finalize_pirt(&pirt_g, &pirt_l);
  ierr = PetscFinalize();
  return ierr;
}
