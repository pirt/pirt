#ifndef PIRT_H
#define PIRT_H

#include <petsctao.h>
#include <fftw3.h>

struct pirt_global_ctx {

  /* global MPI comm */
  PetscMPIInt   global_rank;                    /*! global MPI communicator rank */
  PetscMPIInt   global_size;                    /*! global MPI communicator size */

  /* organizer MPI sub-comm */
  PetscSubcomm  orgpsubcomm;                    /*! PETSc subcommunicator for organizer subcomm */
  MPI_Comm      orgsubcomm;                     /*! MPI subcommunicator for organizer subcomm */
  IS            org_is;                         /*! index set for workflow management across subcomms */
  PetscMPIInt   org_rank;                       /*! organizer subcomm MPI communicator rank */
  PetscMPIInt   org_size;                       /*! organizer subcomm MPI communicator size */

  /* problem dimension descriptors */
  PetscInt      nsubcomms = 1;                  /*! number of task subcommunicators, each of which solves one slice */
  PetscInt      ntheta = -1;                    /*! number of angles */
  PetscInt      ntau = -1;                      /*! size of object */
  PetscInt      nslices = 1;                    /*! number of slices */
  char          sinofile[PETSC_MAX_PATH_LEN];   /*! base name of hdf5 file containing the sinogram */

  /* solver iterations */
  PetscInt      alt_sample_its = 5;             /*! number of (inner) iterations for sample solve */
  PetscInt      alt_shifts_its = 2;             /*! number of (inner) iterations for shifts solve */
  PetscInt      alt_outer_its  = 15;            /*! number of outer iterations for alternating solve */
  PetscInt      overall_sweeps = 1;             /*! number of sweeps over all slices */
  PetscInt      joint_its      = 100;           /*! number of iterations for joint solve */

  /* PIRT options */
  PetscBool     synthetic_data  = PETSC_FALSE;  /*! indicates if synthetic data is being used */
  PetscBool     joint_ec        = PETSC_FALSE;  /*! indicates if joint CoR error correction is enabled */
  PetscBool     alt_ec          = PETSC_FALSE;  /*! indicates if alternating CoR error correction is enabled */
  PetscBool     regularize      = PETSC_FALSE;  /*! indicates if adaptive regularization is enabled */
  PetscBool     normalize_mat   = PETSC_FALSE;  /*! indicates if tomography projection matrix is normalized */
  PetscBool     exact_shifts    = PETSC_FALSE;  /*! indicates if initializing with exact shifts, for sanity check */
  PetscBool     combine_mean    = PETSC_FALSE;  /*! indicates if combining shifts from different slices by mean */
  PetscBool     combine_median  = PETSC_FALSE;  /*! indicates if combining shifts from different slices by median */

  PetscBool     debug           = PETSC_FALSE;  /*! internal flag to enable extra print statements */

  /* global vectors, index sets and scatters */
  Vec           shifts;                         /*! global shifts vector, used to average shifts from task subcomms */
  Vec           shifts_min;                     /*! global shifts vector, used to store max of shifts from task subcomms */
  Vec           shifts_max;                     /*! global shifts vector, used to store min of shifts from task subcomms */
  Vec           gshifts;                        /*! aliased shifts vector made from avearge of shifts on subcoms */
  Vec           exptheta;                       /*! global angles vector to load data and pass on to task subcomms */
  Vec           gexptheta;                      /*! aliased shifts vector of the vector made up from shifts on subcoms */
  IS            ix;                             /*! index sets used to create global to subcomms scatters */
  IS            iy;                             /*! index sets used to create global to subcomms scatters */
  VecScatter    vscat_global_to_tasks;          /*! scatter object that is used to communicate from global to task subcomms */

  /* stages for logging performance */
#if defined(PETSC_USE_LOG)
  PetscLogStage stages[2];                      /*! used for fine grained logging of performance */
#endif

};

struct pirt_local_ctx {

  /* task sub-comm */
  PetscSubcomm  taskpsubcomm;        /*! PETSc subcommunicator for this task */
  MPI_Comm      tasksubcomm;         /*! MPI subcommunicator for this task */
  PetscMPIInt   tasksubcommid;       /*! a unique Int on each task subcomm (max value of global ranks in subcomm) */
  PetscMPIInt   task_rank;           /*! task subcomm MPI communicator rank */
  PetscMPIInt   task_size;           /*! task subcomm MPI communicator size */

  /* workflow tracking */
  PetscInt      total_task_slices;   /*! total number of slices being solved by this task comm */
  PetscInt      curr_task_slice;     /*! current slice being solved by this task comm */
  PetscReal     reg_weight = 0.0;    /*! regularizer weigfht for current sweep */

  /* local copy of problem dimensions */
  PetscInt      ntheta;              /*! number of angles */
  PetscInt      ntau;                /*! size of object */

  /* data distribution descriptors */
  PetscInt      ntheta_local;        /*! number of angles stored on each MPI rank */
  PetscInt      ntau_local;          /*! number of projections stored on each MPI rank */

  /* local copy of solver options */
  PetscBool     joint_ec   = PETSC_FALSE;  /*! indicates if joint CoR error correction is enabled */
  PetscBool     alt_ec     = PETSC_FALSE;  /*! indicates if alternating CoR error correction is enabled */
  PetscBool     regularize = PETSC_FALSE;  /*! indicates if adaptive regularization is enabled */

  /* tomography projection matrix */
  Mat           A;                   /*! tomography projection matrix */

  /* solution, work and optional vectors */
  Vec           recon_sample;        /*! reconstructed sample */
  Vec           shifts;              /*! shifts */
  Vec           expdata;             /*! experimental data, the sinogram! */
  Vec           exptheta;            /*! angles used for scanning the object */
  Vec           smoothed_data;       /*! CoR corrected projections */
  Vec           workvec;             /*! work vector */
  Vec           regvec;              /*! used for regularization of joint solver */
  Vec           ground_truth;        /*! ground truth, if calculating exact norms */

  /* Index sets for joint solve */
  IS            joint_sample_is;     /*! index sets to select the sample vector */
  IS            joint_shifts_is;     /*! index sets to select the shifts vector */

  /* Tao object and bounds vectors for object or combined solve */
  Vec           lowerb;              /*! lower bound vector */
  Vec           upperb;              /*! upper bound vector */
  Tao           tao;                 /*! tao object */

  /* Tao object and bounds vectors for shifts solve */
  Vec           lowerb_s;            /*! lower bound vector for shifts */
  Vec           upperb_s;            /*! upper bound vector for shifts */
  Tao           tao_s;               /*! tao object */

  /* FFT plans and pointers */
  fftw_complex  *data_g;             /*! pointers to fft data */
  fftw_plan     bplan_g;             /*! FFT plans for CoR correction */
  fftw_plan     fplan_g;             /*! FFT plans for CoR correction */
  fftw_complex  *data_b;             /*! pointers to fft data */
  fftw_plan     bplan_b;             /*! FFT plans for CoR correction */
  fftw_plan     fplan_b;             /*! FFT plans for CoR correction */

};

#endif
