/*! \file solver.cxx
 * Routines to setup and run TAO solvers.
 */

#include "solver.hpp"

/* --------------------------------------------------------------------- */
/*!
  Setup solver for the tomography problem, handles the basic no
  center-of-rotation error correction case

  \param *pirt_g  - pirt global context
  \param *pirt_l  - pirt local context
  \return ierr    - PetscErrorCode
  :vs
  */
PetscErrorCode solve_setup(pirt_global_ctx *pirt_g, pirt_local_ctx *pirt_l){

  PetscScalar       sample_lb = 0.0;                     /* lower bound constant for sample */
  PetscScalar       sample_ub = PETSC_INFINITY;          /* upper bound constant for sample */
  Tao               brgn_subsolver;                      /* TAOBRGN subsolver */
  PetscErrorCode    ierr;

  PetscFunctionBeginUser;

  /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
     Create TAO object
     - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  ierr = TaoCreate(pirt_l->tasksubcomm, &(pirt_l->tao)); CHKERRQ(ierr);

  /* Set solver type */
  if (pirt_l->regularize) {
    ierr = TaoSetType(pirt_l->tao, TAOBRGN); CHKERRQ(ierr);
    ierr = TaoBRGNGetSubsolver(pirt_l->tao, &brgn_subsolver);CHKERRQ(ierr);
    ierr = TaoSetType(brgn_subsolver, TAOBQNLS); CHKERRQ(ierr);
    ierr = PetscOptionsSetValue(NULL, "-tao_brgn_regularization_type", "l1dict");CHKERRQ(ierr);
  } else {
    ierr = TaoSetType(pirt_l->tao, TAOBQNLS); CHKERRQ(ierr);
    ierr = TaoSetFromOptions(pirt_l->tao); CHKERRQ(ierr);
  }

  /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
     Setup TAO solvers for no-error-correcting solve
     - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  if (pirt_l->regularize) {
    ierr = TaoSetResidualRoutine(pirt_l->tao, pirt_l->workvec, EvaluateResidual, (void *)pirt_l);CHKERRQ(ierr);
    ierr = TaoSetJacobianResidualRoutine(pirt_l->tao, pirt_l->A, pirt_l->A, EvaluateJacobian,(void*)pirt_l);
  } else {
    ierr = TaoSetObjectiveAndGradientRoutine(pirt_l->tao, FormFunctionGradient, (void *)pirt_l); CHKERRQ(ierr);
  }

  /* set initial vector */
  ierr = TaoSetInitialVector(pirt_l->tao, pirt_l->recon_sample); CHKERRQ(ierr);

  /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
     Create bound vectors for sample and set them
     - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  ierr = VecDuplicate(pirt_l->recon_sample, &(pirt_l->lowerb)); CHKERRQ(ierr);
  ierr = VecDuplicate(pirt_l->recon_sample, &(pirt_l->upperb)); CHKERRQ(ierr);
  ierr = VecSet(pirt_l->lowerb, sample_lb); CHKERRQ(ierr);
  ierr = VecSet(pirt_l->upperb, sample_ub); CHKERRQ(ierr);

  ierr = TaoSetVariableBounds(pirt_l->tao,pirt_l->lowerb,pirt_l->upperb); CHKERRQ(ierr);

  PetscFunctionReturn(ierr);
}

/* --------------------------------------------------------------------- */
/*!
  Execute solver for the tomography problem, handles the basic no
  center-of-rotation error correction cases

  \param *pirt_g  - pirt global context
  \param *pirt_l  - pirt local context
  \return ierr    - PetscErrorCode

*/
PetscErrorCode solve_execute(pirt_global_ctx *pirt_g, pirt_local_ctx *pirt_l){
  PetscErrorCode ierr;
  PetscFunctionBeginUser;

  /* Solve! */
  /* ierr = PetscPrintf(pirt_l->tasksubcomm, "\n Solving...\n");CHKERRQ(ierr); */
  ierr = TaoSolve(pirt_l->tao); CHKERRQ(ierr);

  PetscFunctionReturn(ierr);
}

/* --------------------------------------------------------------------- */
/*!
  Destroy solver for the tomography problem, for the basic no
  center-of-rotation error correction cases

  \param *pirt_g  - pirt global context
  \param *pirt_l  - pirt local context
  \return ierr    - PetscErrorCode

*/
PetscErrorCode solve_destroy(pirt_global_ctx *pirt_g, pirt_local_ctx *pirt_l){

  PetscErrorCode ierr;
  PetscFunctionBeginUser;
  ierr = TaoDestroy(&(pirt_l->tao)); CHKERRQ(ierr);
  ierr = VecDestroy(&(pirt_l->lowerb)); CHKERRQ(ierr);
  ierr = VecDestroy(&(pirt_l->upperb)); CHKERRQ(ierr);
  PetscFunctionReturn(ierr);
}


/* --------------------------------------------------------------------- */
/*!
  Combined routine for evaluating objective function &
  gradient when solving without CoR error correction.

  \param tao    - TAO context
  \param x      - TAO initial/solution vector
  \param *f     - Norm
  \param G      - Gradient vector
  \param ptr    - pirt local context, passed as void pointer
  \return ierr  - PetscErrorCode

*/
PetscErrorCode FormFunctionGradient(Tao tao, Vec x, PetscReal *f, Vec G, void *ptr){

  PetscReal        norm;
  PetscErrorCode   ierr;
  pirt_local_ctx   *pirt_l = (pirt_local_ctx*) ptr;

  PetscFunctionBeginUser;

  /* workvec = A * x */
  ierr = MatMult(pirt_l->A, x, pirt_l->workvec);CHKERRQ(ierr);

  /* workvec = workvec + -1.0 * data) */
  ierr = VecAXPY(pirt_l->workvec, -1.0, pirt_l->expdata);CHKERRQ(ierr);
  ierr = VecNorm(pirt_l->workvec, NORM_2, &norm);CHKERRQ(ierr);

  /* objective function evaluation */
  *f = 0.5*norm*norm;
  /* gradient evaluation routine */
  ierr = MatMultTranspose(pirt_l->A, pirt_l->workvec, G);CHKERRQ(ierr);

  PetscFunctionReturn(ierr);
}

/* --------------------------------------------------------------------- */
/*!
  EvaluateResidual - Residual evaluation routine for least squares
  cost-function. Used for sample solve.

  Input Parameter:
  tao, X, F   - as specified by TAO
  ptr         - pirt_context
  */
PetscErrorCode EvaluateResidual(Tao tao, Vec X, Vec F, void *ptr){

  pirt_local_ctx *pirt = (pirt_local_ctx*)ptr;
  PetscErrorCode ierr;

  PetscFunctionBeginUser;

  /* Compute Ax - b */
  ierr = MatMult(pirt->A, X, F);CHKERRQ(ierr);
  ierr = VecAXPY(F, -1, pirt->expdata);CHKERRQ(ierr);

  PetscFunctionReturn(0);
}

/* --------------------------------------------------------------------- */
/*!
  EvaluateJacobian - Jacobian evaluation routine for least squares
  cost-function. Used for sample solve.

  Input Parameter:
  tao, X, J, Jpre - as specified by TAO
  ptr             - pirt_context
  */
PetscErrorCode EvaluateJacobian(Tao tao_sample, Vec X, Mat J, Mat Jpre, void *ptr){

  PetscFunctionBeginUser;

  /* Empty function since J doesn't change */

  PetscFunctionReturn(0);
}

