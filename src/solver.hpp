#ifndef SOLVER_H
#define SOLVER_H

#include "pirtctx.hpp"

PetscErrorCode solve_setup(pirt_global_ctx *pirt_g, pirt_local_ctx *pirt_l);
PetscErrorCode solve_execute(pirt_global_ctx *pirt_g, pirt_local_ctx *pirt_l);
PetscErrorCode solve_destroy(pirt_global_ctx *pirt_g, pirt_local_ctx *pirt_l);

PetscErrorCode FormFunctionGradient(Tao tao,Vec X,PetscReal *f, Vec G,void *ptr);

PetscErrorCode EvaluateResidual(Tao tao, Vec X, Vec F, void *ptr);
PetscErrorCode EvaluateJacobian(Tao tao_sample, Vec X, Mat J, Mat Jpre, void *ptr);

#endif
