/*! \file solver_alt.cxx
 * Routines to setup and run alternating TAO solvers.
 */

#include "solver_alt.hpp"

/* --------------------------------------------------------------------- */
/*!
  Setup solver for the tomography problem alternating between optimizing
  for shifts and samples.

  \param *pirt_g  - pirt global context
  \param *pirt_l  - pirt local context
  \return ierr    - PetscErrorCode

*/
PetscErrorCode solve_alternating_setup(pirt_global_ctx *pirt_g, pirt_local_ctx *pirt_l){

  PetscInt           sampleits = pirt_g->alt_sample_its;  /* maximum iterations for sample */
  PetscInt           shiftsits = pirt_g->alt_shifts_its;  /* maximum iterations for shifts */
  PetscScalar        shifts_lb = -1*pirt_l->ntau/2.0;     /* lower bound constant for shifts */
  PetscScalar        shifts_ub = pirt_l->ntau/2.0;        /* upper bound constant for shifts */
  PetscScalar        sample_lb = 0.0;                     /* lower bound constant for sample */
  PetscScalar        sample_ub = PETSC_INFINITY;          /* upper bound constant for sample */
  Tao                brgn_subsolver;                      /* TAOBRGN subsolver */
  PetscErrorCode     ierr;
  PetscFunctionBeginUser;

  /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
     Create TAO objects
     - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  ierr = TaoCreate(pirt_l->tasksubcomm, &(pirt_l->tao));CHKERRQ(ierr);
  ierr = TaoCreate(pirt_l->tasksubcomm, &(pirt_l->tao_s));CHKERRQ(ierr);

  /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
     Create bound vectors (for sample & shifts) and set them
     - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  ierr = VecDuplicate(pirt_l->recon_sample, &(pirt_l->lowerb));CHKERRQ(ierr);
  ierr = VecDuplicate(pirt_l->recon_sample, &(pirt_l->upperb));CHKERRQ(ierr);
  ierr = VecSet(pirt_l->lowerb, sample_lb);CHKERRQ(ierr);
  ierr = VecSet(pirt_l->upperb, sample_ub);CHKERRQ(ierr);

  ierr = VecDuplicate(pirt_l->shifts, &(pirt_l->lowerb_s));CHKERRQ(ierr);
  ierr = VecDuplicate(pirt_l->shifts, &(pirt_l->upperb_s));CHKERRQ(ierr);
  ierr = VecSet(pirt_l->lowerb_s, shifts_lb);CHKERRQ(ierr);
  ierr = VecSet(pirt_l->upperb_s, shifts_ub);CHKERRQ(ierr);

  /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
     Initialize sample & shifts to 0
     - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  ierr = VecSet(pirt_l->recon_sample, 0.0);CHKERRQ(ierr);
  if (!pirt_g->exact_shifts) {
    ierr = VecSet(pirt_l->shifts, 0.0);CHKERRQ(ierr);
  }

  /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
     Setup TAO solvers for sample & shifts
     - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  ierr = TaoSetInitialVector(pirt_l->tao, pirt_l->recon_sample); CHKERRQ(ierr);
  ierr = TaoSetInitialVector(pirt_l->tao_s, pirt_l->shifts); CHKERRQ(ierr);
  ierr = TaoSetMaximumIterations(pirt_l->tao, sampleits); CHKERRQ(ierr);
  ierr = TaoSetMaximumIterations(pirt_l->tao_s, shiftsits); CHKERRQ(ierr);
  if (pirt_l->regularize) {
    ierr = TaoSetType(pirt_l->tao, TAOBRGN); CHKERRQ(ierr);
    ierr = TaoBRGNGetSubsolver(pirt_l->tao, &brgn_subsolver);CHKERRQ(ierr);
    ierr = TaoSetType(brgn_subsolver, TAOBQNLS); CHKERRQ(ierr);
    ierr = PetscOptionsSetValue(NULL, "-tao_brgn_regularization_type", "l1dict");CHKERRQ(ierr);
    ierr = TaoSetFromOptions(pirt_l->tao); CHKERRQ(ierr);
  } else {
    ierr = TaoSetType(pirt_l->tao, TAOBQNLS); CHKERRQ(ierr);
    ierr = TaoSetFromOptions(pirt_l->tao); CHKERRQ(ierr);
  }
  ierr = TaoSetType(pirt_l->tao_s,TAOBQNLS); CHKERRQ(ierr);
  ierr = TaoSetFromOptions(pirt_l->tao_s); CHKERRQ(ierr);
  ierr = TaoSetVariableBounds(pirt_l->tao, pirt_l->lowerb, pirt_l->upperb); CHKERRQ(ierr);
  ierr = TaoSetVariableBounds(pirt_l->tao_s, pirt_l->lowerb_s, pirt_l->upperb_s); CHKERRQ(ierr);

  /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
     Set objective function & gradient evaluation routines
     - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  if (pirt_l->regularize) {
    ierr = TaoSetResidualRoutine(pirt_l->tao, pirt_l->workvec, EvaluateResidual, (void *)pirt_l);CHKERRQ(ierr);
    ierr = TaoSetJacobianResidualRoutine(pirt_l->tao, pirt_l->A, pirt_l->A, EvaluateJacobian,(void*)pirt_l);
  } else {
    ierr = TaoSetObjectiveRoutine(pirt_l->tao, FormFunction_Alternating_Sample, (void *)pirt_l); CHKERRQ(ierr);
    ierr = TaoSetGradientRoutine(pirt_l->tao, FormGradient_Alternating_Sample, (void *)pirt_l); CHKERRQ(ierr);
  }

  ierr = TaoSetObjectiveRoutine(pirt_l->tao_s, FormFunction_Alternating_Shifts, (void *)pirt_l); CHKERRQ(ierr);
  ierr = TaoSetGradientRoutine(pirt_l->tao_s, FormGradient_Alternating_Shifts, (void *)pirt_l); CHKERRQ(ierr);

  PetscFunctionReturn(ierr);
}


/* --------------------------------------------------------------------- */
/*!
  Execute solver for the tomography problem alternating between optimizing
  for shifts and samples.

  \param *pirt_g  - pirt global context
  \param *pirt_l  - pirt local context
  \return ierr    - PetscErrorCode

*/
PetscErrorCode solve_alternating_execute(pirt_global_ctx *pirt_g, pirt_local_ctx *pirt_l){

  PetscInt           alt_its = pirt_g->alt_outer_its; /* total number of outer loop iterations */
  PetscInt           i;                               /* loop variable */
  PetscErrorCode     ierr;
  Mat                M_T;                             /* MatLMVM object to force reset, until TAO fixes QN resets */
  Mat                M_T_s;                           /* MatLMVM object to force reset, until TAO fixes QN resets */

  PetscFunctionBeginUser;

  ierr = TaoGetLMVMMatrix(pirt_l->tao_s, &M_T_s);CHKERRQ(ierr);
  if (!pirt_l->regularize) {
    ierr = TaoGetLMVMMatrix(pirt_l->tao, &M_T);CHKERRQ(ierr);
  }

  /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
     Outer solve loop, alternate between solving for shifts and sample
     for each outer loop iteration.
     - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  for (i=0; i<alt_its; i++) {

    /* ierr = PetscPrintf(pirt_l->tasksubcomm, "\n outer iteration number : %d \n", i); */
    /* ierr = PetscPrintf(pirt_l->tasksubcomm, "\n Solving shifts ...\n"); */
    ierr = TaoSolve(pirt_l->tao_s); CHKERRQ(ierr);
    ierr = MatLMVMReset(M_T_s,PETSC_FALSE);CHKERRQ(ierr);

    /* Combine shifts from subcomm's */
    if (pirt_g->combine_mean) {
      ierr = shifts_combine_mean(pirt_g, pirt_l);CHKERRQ(ierr);
    }
    if (pirt_g->combine_median) {
      ierr = shifts_combine_median(pirt_g, pirt_l);CHKERRQ(ierr);
    }

    /* ierr = PetscPrintf(pirt_l->tasksubcomm, "\n Solving sample ...\n"); */
    ierr = TaoSolve(pirt_l->tao); CHKERRQ(ierr);
    if (!pirt_l->regularize) {
      ierr = MatLMVMReset(M_T,PETSC_FALSE);CHKERRQ(ierr);
    }

    if (pirt_g->debug) {
      ierr = PetscBarrier(NULL);CHKERRQ(ierr);
      PetscPrintf(pirt_l->tasksubcomm, "\n post solve shifts...\n");
      ierr = PetscViewerPushFormat(PETSC_VIEWER_STDOUT_WORLD,PETSC_VIEWER_ASCII_DENSE);CHKERRQ(ierr);
      ierr = VecView(pirt_l->shifts, PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr);
      ierr = PetscViewerPopFormat(PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr);
    }
  }

  PetscFunctionReturn(ierr);
}


/* --------------------------------------------------------------------- */
/*!
  Destroy solver for the tomography problem alternating between optimizing
  for shifts and samples.

  \param *pirt_g  - pirt global context
  \param *pirt_l  - pirt local context
  \return ierr    - PetscErrorCode

*/
PetscErrorCode solve_alternating_destroy(pirt_global_ctx *pirt_g, pirt_local_ctx *pirt_l){

  PetscErrorCode     ierr;
  PetscFunctionBeginUser;

  /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
     Destroy bound vectors and TAO objects
     - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  ierr = VecDestroy(&(pirt_l->lowerb)); CHKERRQ(ierr);
  ierr = VecDestroy(&(pirt_l->upperb)); CHKERRQ(ierr);
  ierr = VecDestroy(&(pirt_l->lowerb_s)); CHKERRQ(ierr);
  ierr = VecDestroy(&(pirt_l->upperb_s)); CHKERRQ(ierr);
  ierr = TaoDestroy(&pirt_l->tao); CHKERRQ(ierr);
  ierr = TaoDestroy(&pirt_l->tao_s); CHKERRQ(ierr);

  PetscFunctionReturn(ierr);
}


/* ---------------------------------------------------------------------------------------- */
/*!
  Routine for evaluating gradient when solving for shifts as part of solve_alternating.

  \param tao    - TAO context
  \param x      - TAO initial/solution vector
  \param G      - Gradient vector
  \param ptr    - pirt local context, passed as void pointer
  \return ierr  - PetscErrorCode

*/
PetscErrorCode FormGradient_Alternating_Shifts(Tao tao, Vec x, Vec G, void *ptr){

  PetscInt          i, j, low_idx, high_idx;
  PetscInt          shifts_lowidx, shifts_highidx;
  PetscInt          *idx;
  PetscScalar       *b_array;
  PetscScalar       *shifts, r, Daligned_signal, sum;
  PetscErrorCode    ierr;
  pirt_local_ctx    *pirt_l = (pirt_local_ctx*) ptr;

  PetscFunctionBeginUser;

  ierr = PetscMalloc1(pirt_l->ntheta_local, &shifts);CHKERRQ(ierr);
  ierr = PetscMalloc1(pirt_l->ntheta_local, &idx);CHKERRQ(ierr);

  ierr = convolve(x, pirt_l);

  /* workvec = A * x */
  ierr = MatMult(pirt_l->A, pirt_l->recon_sample, pirt_l->workvec);CHKERRQ(ierr);
  /* workvec = workvec + -1.0 * smoothed_data
     smoothed_data here has been 'aligned' */
  ierr = VecAXPY(pirt_l->workvec, -1.0, pirt_l->smoothed_data);CHKERRQ(ierr);

  ierr = VecGetOwnershipRange(pirt_l->shifts, &shifts_lowidx, &shifts_highidx);  CHKERRQ(ierr);

  ierr = VecGetOwnershipRange(x, &low_idx, &high_idx);  CHKERRQ(ierr);
  ierr = VecGetArray(pirt_l->workvec, &b_array);CHKERRQ(ierr);
  for (i=0; i<pirt_l->ntheta_local; i++) {
    sum = 0;
    for (j=i*pirt_l->ntau; j<((i+1)*pirt_l->ntau); j++) {
      Daligned_signal    = pirt_l->data_g[j][0]/pirt_l->ntau;
      r                  = b_array[j];
      sum = sum - Daligned_signal * r;
    }
    shifts[i] = sum;
    idx[i] = i + low_idx;
    /* PetscPrintf(PETSC_COMM_SELF,"rank/ntheta_local, %d & %d, i : %d, sum : %e \n", pirt_l->rank, pirt_l->ntheta_local, i, sum); */
  }
  ierr = VecRestoreArray(pirt_l->workvec, &b_array);CHKERRQ(ierr);

  ierr = VecSetValues(G, pirt_l->ntheta_local, (PetscInt const*) &idx[0], (PetscScalar const*) &shifts[0], INSERT_VALUES);CHKERRQ(ierr);
  ierr = VecAssemblyBegin(G);CHKERRQ(ierr);
  ierr = VecAssemblyEnd(G);CHKERRQ(ierr);

  ierr = PetscFree(shifts);CHKERRQ(ierr);
  ierr = PetscFree(idx);CHKERRQ(ierr);

  PetscFunctionReturn(ierr);
}

/* ---------------------------------------------------------------------------------------- */
/*!
  Routine for evaluating objective function when solving for shifts as part of solve_alternating

  \param tao    - TAO context
  \param x      - TAO initial/solution vector
  \param *f     - Norm
  \param ptr    - pirt local context, passed as void pointer
  \return ierr  - PetscErrorCode

*/
PetscErrorCode FormFunction_Alternating_Shifts(Tao tao,Vec x,PetscReal *f,void *ptr){

  PetscReal         norm;
  pirt_local_ctx    *pirt_l = (pirt_local_ctx*) ptr;
  PetscErrorCode    ierr;

  PetscFunctionBeginUser;

  ierr = convolve(x, pirt_l);

  /* workvec = A * x */
  ierr = MatMult(pirt_l->A, pirt_l->recon_sample, pirt_l->workvec);CHKERRQ(ierr);

  /* workvec = workvec + -1.0 * smoothed_data,
     smoothed_data here has been 'aligned' */
  ierr = VecAXPY(pirt_l->workvec, -1.0, pirt_l->smoothed_data);CHKERRQ(ierr);
  ierr = VecNorm(pirt_l->workvec, NORM_2, &norm);CHKERRQ(ierr);
  *f = 0.5*norm*norm;

  PetscFunctionReturn(ierr);
}


/* ---------------------------------------------------------------------------------------- */
/*!
  Routine for evaluating gradient when solving for sample as part of solve_alternating

  \param tao    - TAO context
  \param x      - TAO initial/solution vector
  \param G      - Gradient vector
  \param ptr    - pirt local context, passed as void pointer
  \return ierr  - PetscErrorCode

*/
PetscErrorCode FormGradient_Alternating_Sample(Tao tao, Vec x, Vec G, void *ptr){

  PetscErrorCode    ierr;
  pirt_local_ctx    *pirt_l = (pirt_local_ctx*) ptr;

  PetscFunctionBeginUser;

  /* workvec = A * x */
  ierr = MatMult(pirt_l->A, x, pirt_l->workvec);CHKERRQ(ierr);

  /* workvec = workvec + -1.0 * smoothed_data,
     smoothed_data here has been 'aligned' */
  ierr = VecAXPY(pirt_l->workvec, -1.0, pirt_l->smoothed_data);CHKERRQ(ierr);

  /* Get the gradient for the non-rotation parameters */
  ierr = MatMultTranspose(pirt_l->A, pirt_l->workvec, G);CHKERRQ(ierr);

  PetscFunctionReturn(ierr);
}

/* ---------------------------------------------------------------------------------------- */
/*!
  Routine for evaluating objective function when solving for sample as part of solve_alternating

  \param tao    - TAO context
  \param x      - TAO initial/solution vector
  \param *f     - Norm
  \param ptr    - pirt local context, passed as void pointer
  \return ierr  - PetscErrorCode

*/
PetscErrorCode FormFunction_Alternating_Sample(Tao tao, Vec x, PetscReal *f, void *ptr){

  PetscReal         norm;
  pirt_local_ctx    *pirt_l = (pirt_local_ctx*) ptr;
  PetscErrorCode    ierr;

  PetscFunctionBeginUser;

  /* workvec = A * x */
  ierr = MatMult(pirt_l->A, x, pirt_l->workvec);CHKERRQ(ierr);

  /* workvec = workvec + -1.0 * smoothed_data,
     smoothed_data here has been 'aligned' */
  ierr = VecAXPY(pirt_l->workvec, -1.0, pirt_l->smoothed_data);CHKERRQ(ierr);
  ierr = VecNorm(pirt_l->workvec, NORM_2, &norm);CHKERRQ(ierr);
  *f = 0.5*norm*norm;

  PetscFunctionReturn(ierr);
}
