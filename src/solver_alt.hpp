#ifndef SOLVER_ALT_H
#define SOLVER_ALT_H

#include "pirtctx.hpp"
#include "solver.hpp"
#include "convolve.hpp"
#include "subcomm.hpp"

PetscErrorCode solve_alternating_setup(pirt_global_ctx *pirt_g, pirt_local_ctx *pirt_l);
PetscErrorCode solve_alternating_execute(pirt_global_ctx *pirt_g, pirt_local_ctx *pirt_l);
PetscErrorCode solve_alternating_destroy(pirt_global_ctx *pirt_g, pirt_local_ctx *pirt_l);

PetscErrorCode FormGradient_Alternating_Shifts(Tao tao, Vec x, Vec G, void *ptr);
PetscErrorCode FormGradient_Alternating_Sample(Tao tao, Vec x, Vec G, void *ptr);

PetscErrorCode FormFunction_Alternating_Shifts(Tao tao, Vec x, PetscReal *f, void *ptr);
PetscErrorCode FormFunction_Alternating_Sample(Tao tao, Vec x, PetscReal *f, void *ptr);

#endif
