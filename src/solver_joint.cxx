/*! \file solver.cxx
 * Routines to setup and run joint TAO solvers.
 */

#include "solver_joint.hpp"

/* --------------------------------------------------------------------- */
/*!
  Setup solver for the tomography problem for the joint error correction
  error correction case

  \param *pirt_g  - pirt global context
  \param *pirt_l  - pirt local context
  \return ierr    - PetscErrorCode
  */
PetscErrorCode solve_joint_setup(pirt_global_ctx *pirt_g, pirt_local_ctx *pirt_l){

  PetscScalar       shifts_lb = -1*pirt_l->ntau/2.0;     /* lower bound constant for shifts */
  PetscScalar       shifts_ub = pirt_l->ntau/2.0;        /* upper bound constant for shifts */
  PetscScalar       sample_lb = 0.0;                     /* lower bound constant for sample */
  PetscScalar       sample_ub = PETSC_INFINITY;          /* upper bound constant for sample */
  Vec               shifts_vec_lb;
  Vec               shifts_vec_ub;
  Vec               sample_vec;
  PetscErrorCode    ierr;

  PetscFunctionBeginUser;

  /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
     Create TAO object
     - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  ierr = TaoCreate(pirt_l->tasksubcomm, &(pirt_l->tao)); CHKERRQ(ierr);

  /* Get options from command line */
  ierr = TaoSetFromOptions(pirt_l->tao); CHKERRQ(ierr);

  /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
     Setup TAO solvers for joint solve
     - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  ierr = TaoSetObjectiveAndGradientRoutine(pirt_l->tao, FormFunctionGradient_Joint, (void *)pirt_l); CHKERRQ(ierr);
  ierr = TaoSetInitialVector(pirt_l->tao, pirt_l->recon_sample); CHKERRQ(ierr);
  /* Set maximum iterations and solver type*/
  ierr = TaoSetMaximumIterations(pirt_l->tao, pirt_g->joint_its); CHKERRQ(ierr);
  ierr = TaoSetType(pirt_l->tao, TAOBQNLS); CHKERRQ(ierr);

  /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
     Create bound vectors (for sample & shifts) and set them
     - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  ierr = VecDuplicate(pirt_l->recon_sample, &(pirt_l->lowerb)); CHKERRQ(ierr);
  ierr = VecDuplicate(pirt_l->recon_sample, &(pirt_l->upperb)); CHKERRQ(ierr);
  ierr = VecSet(pirt_l->lowerb, sample_lb); CHKERRQ(ierr);
  ierr = VecSet(pirt_l->upperb, sample_ub); CHKERRQ(ierr);

  ierr = VecGetSubVector(pirt_l->lowerb, pirt_l->joint_shifts_is, &(shifts_vec_lb));CHKERRQ(ierr);
  ierr = VecGetSubVector(pirt_l->upperb, pirt_l->joint_shifts_is, &(shifts_vec_ub));CHKERRQ(ierr);
  ierr = VecSet(shifts_vec_lb, shifts_lb); CHKERRQ(ierr);
  ierr = VecSet(shifts_vec_ub, shifts_ub); CHKERRQ(ierr);
  ierr = VecRestoreSubVector(pirt_l->lowerb, pirt_l->joint_shifts_is, &(shifts_vec_lb));CHKERRQ(ierr);
  ierr = VecRestoreSubVector(pirt_l->upperb, pirt_l->joint_shifts_is, &(shifts_vec_ub));CHKERRQ(ierr);

  ierr = TaoSetVariableBounds(pirt_l->tao,pirt_l->lowerb,pirt_l->upperb); CHKERRQ(ierr);

  /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
     Initialize sample & shifts to 0
     - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  if (!pirt_g->exact_shifts) {
    ierr = VecSet(pirt_l->recon_sample, 0.0);CHKERRQ(ierr);
  } else {
    ierr = VecGetSubVector(pirt_l->recon_sample, pirt_l->joint_sample_is, &(sample_vec));CHKERRQ(ierr);
    ierr = VecSet(sample_vec, 0.0);CHKERRQ(ierr);
    ierr = VecRestoreSubVector(pirt_l->recon_sample, pirt_l->joint_sample_is, &(sample_vec));CHKERRQ(ierr);
  }

  /* Regularization vector */
  if (pirt_l->regularize) {
    ierr = VecGetSubVector(pirt_l->recon_sample, pirt_l->joint_sample_is, &(sample_vec));CHKERRQ(ierr);
    ierr = VecDuplicate(sample_vec, &(pirt_l->regvec)); CHKERRQ(ierr);
    ierr = VecRestoreSubVector(pirt_l->recon_sample, pirt_l->joint_sample_is, &(sample_vec));CHKERRQ(ierr);
  }

  PetscFunctionReturn(ierr);
}

/* --------------------------------------------------------------------- */
/*!
  Execute solver for the tomography problem, for the joint
  error correction case

  \param *pirt_g  - pirt global context
  \param *pirt_l  - pirt local context
  \return ierr    - PetscErrorCode

*/
PetscErrorCode solve_joint_execute(pirt_global_ctx *pirt_g, pirt_local_ctx *pirt_l){
  Mat            M_T;               /* MatLMVM object to force reset, until TAO fixes QN resets */
  Vec            joint_shifts_vec;
  PetscErrorCode ierr;
  PetscFunctionBeginUser;


  ierr = TaoGetLMVMMatrix(pirt_l->tao, &M_T);CHKERRQ(ierr);

  /* ierr = PetscPrintf(pirt_l->tasksubcomm, "\n Solving...\n");CHKERRQ(ierr); */
  ierr = TaoSolve(pirt_l->tao); CHKERRQ(ierr);
  ierr = MatLMVMReset(M_T,PETSC_FALSE);CHKERRQ(ierr);

  /* Combine shifts from subcomm's */
  ierr = VecGetSubVector(pirt_l->recon_sample, pirt_l->joint_shifts_is, &(joint_shifts_vec));CHKERRQ(ierr);
  ierr = VecCopy(joint_shifts_vec, pirt_l->shifts);CHKERRQ(ierr);

  if (pirt_g->combine_mean) {
    ierr = shifts_combine_mean(pirt_g, pirt_l);CHKERRQ(ierr);
  }
  if (pirt_g->combine_median) {
    ierr = shifts_combine_median(pirt_g, pirt_l);CHKERRQ(ierr);
  }

  ierr = VecCopy(pirt_l->shifts, joint_shifts_vec);CHKERRQ(ierr);
  ierr = VecRestoreSubVector(pirt_l->recon_sample, pirt_l->joint_shifts_is, &(joint_shifts_vec));CHKERRQ(ierr);

  PetscFunctionReturn(ierr);
}

/* --------------------------------------------------------------------- */
/*!
  Destroy solver for the tomography problem, for the joint
  error correction cases

  \param *pirt_g  - pirt global context
  \param *pirt_l  - pirt local context
  \return ierr    - PetscErrorCode

*/
PetscErrorCode solve_joint_destroy(pirt_global_ctx *pirt_g, pirt_local_ctx *pirt_l){

  PetscErrorCode ierr;
  PetscFunctionBeginUser;
  ierr = TaoDestroy(&(pirt_l->tao)); CHKERRQ(ierr);
  ierr = VecDestroy(&(pirt_l->lowerb)); CHKERRQ(ierr);
  ierr = VecDestroy(&(pirt_l->upperb)); CHKERRQ(ierr);
  PetscFunctionReturn(ierr);
}



/* --------------------------------------------------------------------- */
/*!
  Combined routine for evaluating objective function & gradient when
  solving with joint CoR error correction. Note that the optional
  L1 norm used is the approximated epsL1 version, and the logic is
  similar to the TAO implementation in the BRGN hook.

  \param tao    - TAO context
  \param x      - TAO initial/solution vector
  \param *f     - Norm
  \param G      - Gradient vector
  \param ptr    - pirt local context, passed as void pointer
  \return ierr  - PetscErrorCode

*/
PetscErrorCode FormFunctionGradient_Joint(Tao tao, Vec x, PetscReal *f, Vec G, void *ptr){

  Vec               sample_vec;
  Vec               shifts_vec;
  Vec               sample_grad_vec;
  Vec               shifts_grad_vec;
  PetscInt          i, j;
  PetscReal         epsilon=1e-6;
  PetscReal         regsum;
  PetscScalar const *b_array;
  PetscScalar       *shifts_grad;
  PetscScalar       norm, r, Daligned_signal, sum;
  pirt_local_ctx    *pirt_l = (pirt_local_ctx*) ptr;
  PetscErrorCode    ierr;

  PetscFunctionBeginUser;

  ierr = VecGetSubVector(x, pirt_l->joint_shifts_is, &(shifts_vec));CHKERRQ(ierr);
  ierr = convolve(shifts_vec, pirt_l);
  ierr = VecRestoreSubVector(x, pirt_l->joint_shifts_is, &(shifts_vec));CHKERRQ(ierr);

  /* workvec = A * x */
  ierr = VecGetSubVector(x, pirt_l->joint_sample_is, &sample_vec);CHKERRQ(ierr);
  ierr = MatMult(pirt_l->A, sample_vec, pirt_l->workvec);CHKERRQ(ierr);
  /* eps L1 norm */
  if (pirt_l->regularize) {
    ierr = VecPointwiseMult(pirt_l->regvec, sample_vec, sample_vec);CHKERRQ(ierr);
    ierr = VecShift(pirt_l->regvec, epsilon*epsilon);
    ierr = VecSqrtAbs(pirt_l->regvec);CHKERRQ(ierr);
    ierr = VecSum(pirt_l->regvec, &regsum);CHKERRQ(ierr);
  }
  ierr = VecRestoreSubVector(x, pirt_l->joint_sample_is, &sample_vec);CHKERRQ(ierr);

  /* workvec = workvec + -1.0 * smoothed_data
   *      smoothed_data here has been 'aligned' */
  ierr = VecAXPY(pirt_l->workvec, -1.0, pirt_l->smoothed_data);CHKERRQ(ierr);
  ierr = VecNorm(pirt_l->workvec, NORM_2, &norm);CHKERRQ(ierr);
  *f = 0.5*norm*norm;
  if (pirt_l->regularize) {
    *f = *f +  pirt_l->reg_weight*(regsum - (pirt_l->ntau*pirt_l->ntau)*epsilon);
  }

  /* Compute and set gradient for the sample */
  ierr = VecGetSubVector(G, pirt_l->joint_sample_is, &sample_grad_vec);CHKERRQ(ierr);
  ierr = MatMultTranspose(pirt_l->A, pirt_l->workvec, sample_grad_vec);CHKERRQ(ierr);
  /* gradient of epsL1 norm */
  if (pirt_l->regularize) {
    ierr = VecGetSubVector(x, pirt_l->joint_sample_is, &sample_vec);CHKERRQ(ierr);
    ierr = VecPointwiseDivide(pirt_l->regvec, sample_vec, pirt_l->regvec);CHKERRQ(ierr);
    ierr = VecAXPY(sample_grad_vec, pirt_l->reg_weight, pirt_l->regvec);CHKERRQ(ierr);
    ierr = VecRestoreSubVector(x, pirt_l->joint_sample_is, &sample_vec);CHKERRQ(ierr);
  }
  ierr = VecRestoreSubVector(G, pirt_l->joint_sample_is, &sample_grad_vec);CHKERRQ(ierr);

  /* Compute & set gradient for the shifts */
  ierr = VecGetSubVector(G, pirt_l->joint_shifts_is, &shifts_grad_vec);CHKERRQ(ierr);
  ierr = VecGetArray(shifts_grad_vec, &shifts_grad);CHKERRQ(ierr);
  ierr = VecGetArrayRead(pirt_l->workvec, &b_array);CHKERRQ(ierr);
  for (i=0; i<pirt_l->ntheta_local; i++) {
    sum = 0;
    for (j=i*pirt_l->ntau; j<((i+1)*pirt_l->ntau); j++) {
      Daligned_signal    = pirt_l->data_g[j][0]/pirt_l->ntau;
      r                  = b_array[j];
      sum = sum - Daligned_signal * r;
    }
    shifts_grad[i] = sum;
    /* PetscPrintf(PETSC_COMM_SELF,"rank/ntheta_local, %d & %d, i : %d, sum : %e \n", pirt_l->rank, pirt_l->ntheta_local, i, sum); */
  }
  ierr = VecRestoreArrayRead(pirt_l->workvec, &b_array);CHKERRQ(ierr);
  ierr = VecRestoreArray(shifts_grad_vec, &shifts_grad);CHKERRQ(ierr);
  ierr = VecRestoreSubVector(G, pirt_l->joint_shifts_is, &shifts_grad_vec);CHKERRQ(ierr);

  ierr = VecGetArray(G, &shifts_grad);CHKERRQ(ierr);
  ierr = VecRestoreArray(G, &shifts_grad);CHKERRQ(ierr);

  PetscFunctionReturn(ierr);
}

