#ifndef SOLVER_JOINT_H
#define SOLVER_JOINT_H

#include "pirtctx.hpp"
#include "convolve.hpp"
#include "subcomm.hpp"

PetscErrorCode solve_joint_setup(pirt_global_ctx *pirt_g, pirt_local_ctx *pirt_l);
PetscErrorCode solve_joint_execute(pirt_global_ctx *pirt_g, pirt_local_ctx *pirt_l);
PetscErrorCode solve_joint_destroy(pirt_global_ctx *pirt_g, pirt_local_ctx *pirt_l);

PetscErrorCode FormFunctionGradient_Joint(Tao tao, Vec x, PetscReal *f, Vec G, void *ptr);

#endif
