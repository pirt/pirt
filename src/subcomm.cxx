/*! \file subcomm.cxx
 * Routines to handle sub-communcators : creation & scatters.
 */

#include "subcomm.hpp"

/* --------------------------------------------------------------------- */
/*!
  Initialize task subcomms

  \param *pirt_g  - pirt global context
  \param *pirt_l  - pirt local context
  \return ierr    - PetscErrorCode

*/
PetscErrorCode init_tasksubcomms(pirt_global_ctx *pirt_g, pirt_local_ctx *pirt_l) {

  PetscErrorCode ierr;

  PetscFunctionBeginUser;
  ierr = PetscSubcommCreate(PETSC_COMM_WORLD, &(pirt_l->taskpsubcomm));CHKERRQ(ierr);
  ierr = PetscSubcommSetNumber(pirt_l->taskpsubcomm, pirt_g->nsubcomms);CHKERRQ(ierr);
  ierr = PetscSubcommSetType(pirt_l->taskpsubcomm, PETSC_SUBCOMM_CONTIGUOUS);CHKERRQ(ierr);

  ierr = PetscSubcommGetChild(pirt_l->taskpsubcomm, &(pirt_l->tasksubcomm));CHKERRQ(ierr);
  ierr = MPIU_Allreduce(&(pirt_g->global_rank), &(pirt_l->tasksubcommid), 1, MPIU_INT, MPI_MAX, pirt_l->tasksubcomm);CHKERRQ(ierr);
  ierr = MPI_Comm_rank(pirt_l->tasksubcomm, &(pirt_l->task_rank));CHKERRQ(ierr);
  ierr = MPI_Comm_size(pirt_l->tasksubcomm, &(pirt_l->task_size));CHKERRQ(ierr);
  PetscFunctionReturn(ierr);
}

/* --------------------------------------------------------------------- */
/*!
  Initialize organizer subcomms

  \param *pirt_g  - pirt global context
  \param *pirt_l  - pirt local context
  \return ierr    - PetscErrorCode

*/
PetscErrorCode init_organizersubcomms(pirt_global_ctx *pirt_g, pirt_local_ctx *pirt_l ) {

  PetscErrorCode ierr;
  PetscMPIInt    color, subrank;

  PetscFunctionBeginUser;
  if (pirt_l->task_rank == 0) {
    color = 0;
  } else {
    color = 1;
  }
  subrank = pirt_g->global_rank/pirt_l->task_size;
  if (pirt_g->debug) {
    ierr = PetscPrintf(PETSC_COMM_SELF, "taskrank : %d, globalrank : %d, color : %d, subrank : %d\n",
        (PetscInt)pirt_l->task_rank, (PetscInt)pirt_g->global_rank, (PetscInt)color, (PetscInt)subrank);CHKERRQ(ierr);
  }
  ierr = PetscSubcommCreate(PETSC_COMM_WORLD, &(pirt_g->orgpsubcomm));CHKERRQ(ierr);
  ierr = PetscSubcommSetTypeGeneral(pirt_g->orgpsubcomm, color, subrank); CHKERRQ(ierr);
  ierr = PetscSubcommGetChild(pirt_g->orgpsubcomm, &(pirt_g->orgsubcomm));CHKERRQ(ierr);
  PetscFunctionReturn(ierr);
}

/* --------------------------------------------------------------------- */
/*!
  Initialize organizer index set

  \param *pirt_g  - pirt global context
  \param *pirt_l  - pirt local context
  \return ierr    - PetscErrorCode

*/
PetscErrorCode init_organizeris(pirt_global_ctx *pirt_g, pirt_local_ctx *pirt_l ) {

  PetscLayout    ismap;
  PetscInt       islow, ishigh;
  PetscErrorCode ierr;
  PetscFunctionBeginUser;
  ierr = PetscBarrier(NULL);CHKERRQ(ierr);
  if (pirt_l->task_rank==0) {
    ierr = PetscLayoutCreateFromSizes(pirt_g->orgsubcomm, PETSC_DECIDE, pirt_g->nslices, (PetscInt)1, &ismap);CHKERRQ(ierr);
    ierr = PetscLayoutGetRange(ismap, &islow, &ishigh);CHKERRQ(ierr);
    pirt_l->total_task_slices = ishigh - islow;
    ierr = PetscPrintf(PETSC_COMM_SELF, "total_task_slices : %d on taskcommid %d \n", pirt_l->total_task_slices, pirt_l->tasksubcommid);CHKERRQ(ierr);
    ierr = ISCreateStride(pirt_g->orgsubcomm, pirt_l->total_task_slices, islow, 1, &(pirt_g->org_is));CHKERRQ(ierr);
    ierr = PetscLayoutDestroy(&ismap);CHKERRQ(ierr);
    if (pirt_g->debug) ierr = ISView(pirt_g->org_is, PETSC_VIEWER_STDOUT_(pirt_g->orgsubcomm));CHKERRQ(ierr);
  }
  ierr = PetscBarrier(NULL);CHKERRQ(ierr);
  ierr = MPI_Bcast(&(pirt_l->total_task_slices), 1, MPIU_INT, 0, pirt_l->tasksubcomm);CHKERRQ(ierr);

  PetscFunctionReturn(ierr);
}

/* --------------------------------------------------------------------- */
/*!
  Initialize vecscatter object to manage
  global to tasks scatters.

  \note - This is adapted from the world2subs case in
  $(PETSC_DIR)/src/vec/is/sf/tests/ex9.c

  \param *pirt_g  - pirt global context
  \param *pirt_l  - pirt local context
  \return ierr    - PetscErrorCode

*/
PetscErrorCode init_global_to_tasks_scatters(pirt_global_ctx *pirt_g, pirt_local_ctx *pirt_l) {

  PetscInt      ntheta_task_local;
  PetscScalar   *theta_task_val;
  PetscInt      xstart, ystart;

  PetscErrorCode ierr;
  PetscFunctionBeginUser;

  ierr = VecGetLocalSize(pirt_l->exptheta, &ntheta_task_local);CHKERRQ(ierr);

  ierr = VecGetArray(pirt_l->exptheta, &theta_task_val);CHKERRQ(ierr);
  ierr = VecCreateMPIWithArray(PETSC_COMM_WORLD,1,
      ntheta_task_local,
      PETSC_DECIDE,
      theta_task_val,
      &(pirt_g->gexptheta));CHKERRQ(ierr);
  ierr = PetscObjectSetName((PetscObject)(pirt_g->gexptheta), "exptheta_task_on_subcomms");CHKERRQ(ierr); /* Give a name to view yg clearly */
  ierr = VecRestoreArray(pirt_l->exptheta, &theta_task_val);CHKERRQ(ierr);

  ierr = VecGetArray(pirt_l->shifts, &theta_task_val);CHKERRQ(ierr);
  ierr = VecCreateMPIWithArray(PETSC_COMM_WORLD,1,
      ntheta_task_local,
      PETSC_DECIDE,
      theta_task_val,
      &(pirt_g->gshifts));CHKERRQ(ierr);
  ierr = PetscObjectSetName((PetscObject)(pirt_g->gshifts), "shifts_task_on_subcomms");CHKERRQ(ierr); /* Give a name to view yg clearly */
  ierr = VecRestoreArray(pirt_l->shifts, &theta_task_val);CHKERRQ(ierr);

  ierr = VecGetOwnershipRange(pirt_l->exptheta, &xstart,NULL);CHKERRQ(ierr);
  ierr = VecGetOwnershipRange(pirt_g->gexptheta, &ystart,NULL);CHKERRQ(ierr);

  ierr = ISCreateStride(PETSC_COMM_SELF, ntheta_task_local, xstart, 1, &pirt_g->ix);CHKERRQ(ierr);
  ierr = ISCreateStride(PETSC_COMM_SELF, ntheta_task_local, ystart, 1, &pirt_g->iy);CHKERRQ(ierr);

  /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
     Create the scatter
     - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  ierr = VecScatterCreate(pirt_g->exptheta, pirt_g->ix,
      pirt_g->gexptheta, pirt_g->iy,
      &pirt_g->vscat_global_to_tasks);CHKERRQ(ierr);

  /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
     Create extra global shifts vectors for when combining via median
     - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  if (pirt_g->combine_median){
    ierr = VecDuplicate(pirt_g->shifts, &(pirt_g->shifts_min));CHKERRQ(ierr);
    ierr = VecDuplicate(pirt_g->shifts, &(pirt_g->shifts_max));CHKERRQ(ierr);
  }

  PetscFunctionReturn(ierr);
}

/* --------------------------------------------------------------------- */
/*!

  Combine shifts from subcomms via mean. Note: for joint error correction,
  we extract the shifts from the joint reconstruction vector, copy it to
  the shifts vector, and reverse the process after combining local shifts.

  \param *pirt_g  - pirt global context
  \param *pirt_l  - pirt local context
  \return ierr    - PetscErrorCode

*/
PetscErrorCode shifts_combine_mean(pirt_global_ctx *pirt_g, pirt_local_ctx *pirt_l ) {
  PetscErrorCode ierr;
  PetscFunctionBeginUser;

  /* add all shifts to global shifts vector and average to get mean */
  ierr = VecSet(pirt_g->shifts, 0.0);CHKERRQ(ierr);
  ierr = VecScatterBegin(pirt_g->vscat_global_to_tasks,
      pirt_g->gshifts, pirt_g->shifts,
      ADD_VALUES, SCATTER_REVERSE);CHKERRQ(ierr);
  ierr = VecScatterEnd(pirt_g->vscat_global_to_tasks,
      pirt_g->gshifts, pirt_g->shifts,
      ADD_VALUES, SCATTER_REVERSE);CHKERRQ(ierr);
  ierr = VecScale(pirt_g->shifts, (PetscScalar)(1.0/pirt_g->nsubcomms));CHKERRQ(ierr);


  if (pirt_g->debug) {
    PetscPrintf(pirt_l->tasksubcomm, "\n combined shifts are ...\n");
    ierr = PetscViewerPushFormat(PETSC_VIEWER_STDOUT_WORLD,PETSC_VIEWER_ASCII_DENSE);CHKERRQ(ierr);
    ierr = VecView(pirt_g->shifts, PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr);
    ierr = PetscViewerPopFormat(PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr);
  }

  ierr = VecScatterBegin(pirt_g->vscat_global_to_tasks,
      pirt_g->shifts, pirt_g->gshifts,
      INSERT_VALUES, SCATTER_FORWARD);CHKERRQ(ierr);
  ierr = VecScatterEnd(pirt_g->vscat_global_to_tasks,
      pirt_g->shifts, pirt_g->gshifts,
      INSERT_VALUES, SCATTER_FORWARD);CHKERRQ(ierr);

  PetscFunctionReturn(ierr);
}

/* --------------------------------------------------------------------- */
/*!

  Combine shifts from subcomms via median. Note: for joint error correction,
  we extract the shifts from the joint reconstruction vector, copy it to
  the shifts vector, and reverse the process after combining local shifts.

  \param *pirt_g  - pirt global context
  \param *pirt_l  - pirt local context
  \return ierr    - PetscErrorCode

*/
PetscErrorCode shifts_combine_median(pirt_global_ctx *pirt_g, pirt_local_ctx *pirt_l ) {
  PetscErrorCode ierr;
  PetscFunctionBeginUser;

  /* add all shifts to global shifts vector and average to get mean */
  ierr = VecSet(pirt_g->shifts, 0.0);CHKERRQ(ierr);
  ierr = VecScatterBegin(pirt_g->vscat_global_to_tasks,
      pirt_g->gshifts, pirt_g->shifts,
      ADD_VALUES, SCATTER_REVERSE);CHKERRQ(ierr);
  ierr = VecScatterEnd(pirt_g->vscat_global_to_tasks,
      pirt_g->gshifts, pirt_g->shifts,
      ADD_VALUES, SCATTER_REVERSE);CHKERRQ(ierr);
  ierr = VecScale(pirt_g->shifts, (PetscScalar)(1.0/pirt_g->nsubcomms));CHKERRQ(ierr);

  /* get mean, max and min of shifts from subcomms, estimate median from them
     as median = 2*mean - 0.5*(min+max) */
  ierr = VecSet(pirt_g->shifts, 0.0);CHKERRQ(ierr);
  ierr = VecSet(pirt_g->shifts_min, 0.0);CHKERRQ(ierr);
  ierr = VecSet(pirt_g->shifts_max, 0.0);CHKERRQ(ierr);
  ierr = VecScatterBegin(pirt_g->vscat_global_to_tasks,
      pirt_g->gshifts, pirt_g->shifts,
      ADD_VALUES, SCATTER_REVERSE);CHKERRQ(ierr);
  ierr = VecScatterEnd(pirt_g->vscat_global_to_tasks,
      pirt_g->gshifts, pirt_g->shifts,
      ADD_VALUES, SCATTER_REVERSE);CHKERRQ(ierr);
  ierr = VecScatterBegin(pirt_g->vscat_global_to_tasks,
      pirt_g->gshifts, pirt_g->shifts_min,
      MIN_VALUES, SCATTER_REVERSE);CHKERRQ(ierr);
  ierr = VecScatterEnd(pirt_g->vscat_global_to_tasks,
      pirt_g->gshifts, pirt_g->shifts_min,
      MIN_VALUES, SCATTER_REVERSE);CHKERRQ(ierr);
  ierr = VecScatterBegin(pirt_g->vscat_global_to_tasks,
      pirt_g->gshifts, pirt_g->shifts_max,
      MAX_VALUES, SCATTER_REVERSE);CHKERRQ(ierr);
  ierr = VecScatterEnd(pirt_g->vscat_global_to_tasks,
      pirt_g->gshifts, pirt_g->shifts_max,
      MAX_VALUES, SCATTER_REVERSE);CHKERRQ(ierr);
  ierr = VecScale(pirt_g->shifts, (PetscScalar)(1.0/pirt_g->nsubcomms));CHKERRQ(ierr);
  ierr = VecAXPBYPCZ(pirt_g->shifts, -0.5, -0.5, 2.0, pirt_g->shifts_min, pirt_g->shifts_max);CHKERRQ(ierr);

  if (pirt_g->debug) {
    PetscPrintf(pirt_l->tasksubcomm, "\n combined shifts are ...\n");
    ierr = PetscViewerPushFormat(PETSC_VIEWER_STDOUT_WORLD,PETSC_VIEWER_ASCII_DENSE);CHKERRQ(ierr);
    ierr = VecView(pirt_g->shifts, PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr);
    ierr = PetscViewerPopFormat(PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr);
  }

  ierr = VecScatterBegin(pirt_g->vscat_global_to_tasks,
      pirt_g->shifts, pirt_g->gshifts,
      INSERT_VALUES, SCATTER_FORWARD);CHKERRQ(ierr);
  ierr = VecScatterEnd(pirt_g->vscat_global_to_tasks,
      pirt_g->shifts, pirt_g->gshifts,
      INSERT_VALUES, SCATTER_FORWARD);CHKERRQ(ierr);

  PetscFunctionReturn(ierr);
}

