#ifndef SUBCOMM_H
#define SUBCOMM_H

#include "pirtctx.hpp"

PetscErrorCode init_tasksubcomms(pirt_global_ctx *pirt_g, pirt_local_ctx *pirt_l);
PetscErrorCode init_organizersubcomms(pirt_global_ctx *pirt_g, pirt_local_ctx *pirt_l);

PetscErrorCode init_organizeris(pirt_global_ctx *pirt_g, pirt_local_ctx *pirt_l);

PetscErrorCode init_global_to_tasks_scatters(pirt_global_ctx *pirt_g, pirt_local_ctx *pirt_l);
PetscErrorCode shifts_combine_mean(pirt_global_ctx *pirt_g, pirt_local_ctx *pirt_l );
PetscErrorCode shifts_combine_median(pirt_global_ctx *pirt_g, pirt_local_ctx *pirt_l );

#endif
