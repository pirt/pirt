/*! \file utils.cxx
 * Utilities and misc functions
 */

#include "utils.hpp"

/* --------------------------------------------------------------------- */
/*!
  Parse commands, load data, initialize geometry
  and make fftw plans.

  \param argc    - command line inputs
  \param **args  - command line inputs
  \param *pirt_g - pirt global context
  \param *pirt_l - pirt local context
  \param *det    - detector geometry context
  \return ierr   - PetscErrorCode

*/
PetscErrorCode initialize_pirt(int argc, char **args,
    pirt_global_ctx *pirt_g,
    pirt_local_ctx  *pirt_l,
    detctx *det){

  /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
     Initialize PETSc and MPI, query mpi rank and size
     - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  PetscInitialize(&argc,&args,(char*)0,NULL);
  MPI_Comm_rank(PETSC_COMM_WORLD,&pirt_g->global_rank);
  MPI_Comm_size(PETSC_COMM_WORLD,&pirt_g->global_size);

  PetscInt       i;
  PetscReal      tol = 1;
  PetscReal      omega[4] = {-2, 2, -2, 2};
  PetscInt       m[2];
  char           anglefile[PETSC_MAX_PATH_LEN];
  char           exactshiftsfile[PETSC_MAX_PATH_LEN];
  PetscBool      angleflg;
  PetscBool      sinoflg;
  PetscBool      exactshiftsflg;
  PetscErrorCode ierr;

  PetscLogStageRegister("mat create",&pirt_g->stages[0]);
  PetscLogStageRegister("solve",&pirt_g->stages[1]);

  PetscPrintf(PETSC_COMM_WORLD,"PIRT -- Parallel Iterative Reconstruction Tomography\n");

  /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
     Parse options list
     - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  ierr = PetscOptionsGetReal(NULL,NULL,"-box_scale", &tol, NULL);CHKERRQ(ierr);
  ierr = PetscOptionsGetBool(NULL,NULL,"-synthetic", &pirt_g->synthetic_data, NULL);CHKERRQ(ierr);
  ierr = PetscOptionsGetBool(NULL,NULL,"-joint", &pirt_g->joint_ec, NULL);CHKERRQ(ierr);
  ierr = PetscOptionsGetBool(NULL,NULL,"-alternating", &pirt_g->alt_ec, NULL);CHKERRQ(ierr);
  ierr = PetscOptionsGetBool(NULL,NULL,"-regularize", &pirt_g->regularize, NULL);CHKERRQ(ierr);
  ierr = PetscOptionsGetBool(NULL,NULL,"-normalize_mat", &pirt_g->normalize_mat, NULL);CHKERRQ(ierr);
  ierr = PetscOptionsGetBool(NULL,NULL,"-exact_shifts", &pirt_g->exact_shifts, NULL);CHKERRQ(ierr);
  ierr = PetscOptionsGetBool(NULL,NULL,"-combine_mean", &pirt_g->combine_mean, NULL);CHKERRQ(ierr);
  ierr = PetscOptionsGetBool(NULL,NULL,"-combine_median", &pirt_g->combine_median, NULL);CHKERRQ(ierr);

  ierr = PetscOptionsGetInt(NULL,NULL,"-overall_sweeps", &(pirt_g->overall_sweeps), NULL);CHKERRQ(ierr);
  ierr = PetscOptionsGetInt(NULL,NULL,"-alt_outer_its", &(pirt_g->alt_outer_its), NULL);CHKERRQ(ierr);
  ierr = PetscOptionsGetInt(NULL,NULL,"-alt_sample_its", &(pirt_g->alt_sample_its), NULL);CHKERRQ(ierr);
  ierr = PetscOptionsGetInt(NULL,NULL,"-alt_shifts_its", &(pirt_g->alt_shifts_its), NULL);CHKERRQ(ierr);
  ierr = PetscOptionsGetInt(NULL,NULL,"-joint_its", &(pirt_g->joint_its), NULL);CHKERRQ(ierr);
  ierr = PetscOptionsGetInt(NULL,NULL,"-ntheta", &(pirt_g->ntheta), NULL);CHKERRQ(ierr);
  ierr = PetscOptionsGetInt(NULL,NULL,"-ntau", &(pirt_g->ntau), NULL);CHKERRQ(ierr);
  ierr = PetscOptionsGetInt(NULL,NULL,"-nslices", &(pirt_g->nslices), NULL);CHKERRQ(ierr);
  ierr = PetscOptionsGetInt(NULL,NULL,"-nsubcomms", &(pirt_g->nsubcomms), NULL);CHKERRQ(ierr);

  if (pirt_g->ntau*pirt_g->ntheta < 0) SETERRQ(PETSC_COMM_WORLD, PETSC_ERR_USER,
      "Please provide ntheta (number of projection anges) and ntau (object size) parameters!");

  /*Copy some solver flags into local context*/
  pirt_l->ntheta     = pirt_g->ntheta;
  pirt_l->ntau       = pirt_g->ntau;
  pirt_l->joint_ec   = pirt_g->joint_ec;
  pirt_l->alt_ec     = pirt_g->alt_ec;
  pirt_l->regularize = pirt_g->regularize;

  ierr = init_tasksubcomms(pirt_g, pirt_l);CHKERRQ(ierr);
  ierr = init_organizersubcomms(pirt_g, pirt_l);CHKERRQ(ierr);
  ierr = init_organizeris(pirt_g, pirt_l);CHKERRQ(ierr);

  ierr = PetscOptionsGetString(NULL,NULL, "-anglefile",
      anglefile, PETSC_MAX_PATH_LEN, &angleflg);CHKERRQ(ierr);

  if (!angleflg) SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_USER,
      "Please specify the input hdf5 angles file with -anglefile ...\n");

  ierr = PetscOptionsGetString(NULL,NULL, "-sinofile",
      pirt_g->sinofile, PETSC_MAX_PATH_LEN, &sinoflg);CHKERRQ(ierr);

  if (!sinoflg && !pirt_g->synthetic_data) SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_USER,
      "Please specify the input hdf5 datafile with -sinofile ...\n");

  ierr = PetscOptionsGetString(NULL,NULL, "-exactshiftsfile",
      exactshiftsfile, PETSC_MAX_PATH_LEN, &exactshiftsflg);CHKERRQ(ierr);

  if (!exactshiftsflg && pirt_g->exact_shifts) SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_USER,
      "Please specify the exact shifts hdf5 datafile with -exactshiftsfile ...\n");

  if (pirt_g->synthetic_data && pirt_g->nsubcomms!=1) SETERRQ(PETSC_COMM_WORLD, PETSC_ERR_USER,
      "Synthetic data only works on 1 subcomm");

  if (pirt_g->exact_shifts && pirt_g->nsubcomms!=1) SETERRQ(PETSC_COMM_WORLD, PETSC_ERR_USER,
      "Exact shifts initialization only works on 1 subcomm");

  if (pirt_g->exact_shifts) ierr = PetscPrintf(PETSC_COMM_WORLD, "Using Exact shifts!\n");CHKERRQ(ierr);
  if (pirt_g->alt_ec) ierr = PetscPrintf(PETSC_COMM_WORLD,"Using Alternating CoR Error Correction !\n");CHKERRQ(ierr);
  if (pirt_g->joint_ec) ierr = PetscPrintf(PETSC_COMM_WORLD,"Using Joint CoR Error Correction !\n");CHKERRQ(ierr);

  /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
     Create and setup parallel data structures (Mat/Vecs)
     - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  ierr = init_layouts(pirt_g, pirt_l);CHKERRQ(ierr);

  if (pirt_l->joint_ec){
    ierr = joint_is_init(pirt_l);CHKERRQ(ierr);
  }

  /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
     Create scatter from global to task subcomms
     - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  ierr = init_global_to_tasks_scatters(pirt_g, pirt_l);CHKERRQ(ierr);

  /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
     Load angle data from h5 file into exptheta
     - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  PetscPrintf(PETSC_COMM_WORLD,"Reading in angles data from %s\n",anglefile);
  ierr = load_angles(anglefile, pirt_g);CHKERRQ(ierr);

  /* Optinally load exact shifts */
  if (pirt_g->exact_shifts) {
    ierr = load_exact_shifts(exactshiftsfile, pirt_g, pirt_l);CHKERRQ(ierr);
  }

  /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
     Initialize fftw plans and allocate data for their
     execution and state storage
     - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  if (pirt_g->alt_ec || pirt_g->joint_ec) {
    const int n[] = {(int)pirt_g->ntau}; /* 1d transforms of length ntau */
    int stride, dist;
    pirt_l->data_b  = fftw_alloc_complex((size_t)((int)pirt_l->ntheta_local*(int)pirt_g->ntau));
    pirt_l->data_g  = fftw_alloc_complex((size_t)((int)pirt_l->ntheta_local*(int)pirt_g->ntau));
    stride = 1;
    dist   = (int) pirt_g->ntau;
    pirt_l->fplan_b = fftw_plan_many_dft(1, n, (int)pirt_l->ntheta_local,
        pirt_l->data_b, NULL, stride, dist,
        pirt_l->data_b, NULL, stride, dist,
        FFTW_FORWARD, FFTW_ESTIMATE);
    pirt_l->bplan_b = fftw_plan_many_dft(1, n, (int)pirt_l->ntheta_local,
        pirt_l->data_b, NULL, stride, dist,
        pirt_l->data_b, NULL, stride, dist,
        FFTW_BACKWARD, FFTW_ESTIMATE);
    pirt_l->fplan_g = fftw_plan_many_dft(1, n, (int)pirt_l->ntheta_local,
        pirt_l->data_g, NULL, stride, dist,
        pirt_l->data_g, NULL, stride, dist,
        FFTW_FORWARD, FFTW_ESTIMATE);
    pirt_l->bplan_g = fftw_plan_many_dft(1, n, (int)pirt_l->ntheta_local,
        pirt_l->data_g, NULL, stride, dist,
        pirt_l->data_g, NULL, stride, dist,
        FFTW_BACKWARD, FFTW_ESTIMATE);

    ierr = VecDuplicate(pirt_l->expdata,&(pirt_l->smoothed_data));CHKERRQ(ierr);

    PetscPrintf(PETSC_COMM_WORLD,"CoR initialization done!\n");
  }

  /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
     Initialize detector geometry context
     - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  det->numThetan        = (int) pirt_g->ntheta;
  det->ntau             = (int) pirt_g->ntau;
  det->Tol              = (double) tol;

  m[0] = pirt_g->ntau; m[1] = pirt_g->ntau;
  for (i=0;i<2;i++) {det->m[i] = (double) m[i];}
  for (i=0;i<4;i++) {det->omega[i] = (double) (omega[i]*det->Tol);}

  init_detctx(det, pirt_g);

  PetscPrintf(PETSC_COMM_WORLD,"After detector geometry context initialization\n");

  PetscFunctionReturn(0);
}


/* --------------------------------------------------------------------- */
/*!
  Destroy all data structures and free fftw memory.

  \param *pirt_g - pirt global context
  \param *pirt_l - pirt local context
  \return ierr   - PetscErrorCode

*/
PetscErrorCode finalize_pirt(pirt_global_ctx *pirt_g, pirt_local_ctx *pirt_l){

  PetscErrorCode ierr;

  PetscFunctionBeginUser;

  /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
     Destroy common data structures used with and without CoR error correction
     - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  /* local data structures */
  ierr = MatDestroy(&(pirt_l->A));CHKERRQ(ierr);
  ierr = VecDestroy(&(pirt_l->recon_sample));CHKERRQ(ierr);
  ierr = VecDestroy(&(pirt_l->expdata));CHKERRQ(ierr);
  ierr = VecDestroy(&(pirt_l->exptheta));CHKERRQ(ierr);
  ierr = VecDestroy(&(pirt_l->workvec));CHKERRQ(ierr);
  ierr = VecDestroy(&(pirt_l->shifts));CHKERRQ(ierr);

  /* global data structures */
  ierr = VecDestroy(&(pirt_g->exptheta));CHKERRQ(ierr);
  ierr = VecDestroy(&(pirt_g->shifts));CHKERRQ(ierr);
  ierr = ISDestroy(&(pirt_g->ix));CHKERRQ(ierr);
  ierr = ISDestroy(&(pirt_g->iy));CHKERRQ(ierr);
  ierr = VecScatterDestroy(&(pirt_g->vscat_global_to_tasks));CHKERRQ(ierr);

  /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
     Destroy common data structures and fftw plans specific CoR error correction
     - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  if (pirt_g->alt_ec || pirt_g->joint_ec) {
    ierr = VecDestroy(&(pirt_l->smoothed_data));CHKERRQ(ierr);
    fftw_free(pirt_l->data_b);
    fftw_free(pirt_l->data_g);
    fftw_destroy_plan(pirt_l->fplan_b);
    fftw_destroy_plan(pirt_l->bplan_b);
    fftw_destroy_plan(pirt_l->fplan_g);
    fftw_destroy_plan(pirt_l->bplan_g);
  }

  /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
     Destroy utility index sets used for joint reconstruction
     - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  if (pirt_l->joint_ec) {
    ierr = ISDestroy(&(pirt_l->joint_sample_is));CHKERRQ(ierr);
    ierr = ISDestroy(&(pirt_l->joint_shifts_is));CHKERRQ(ierr);
  }

  /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
     Destroy organizer IS used for workflow management
     - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  if (pirt_l->task_rank==0) {
    ierr = ISDestroy(&(pirt_g->org_is));CHKERRQ(ierr);
  }
  ierr = PetscBarrier(NULL);CHKERRQ(ierr);

  PetscFunctionReturn(ierr);
}

/* --------------------------------------------------------------------- */
/*!
  Initialize index sets for joint solve.

  \param *pirt_l - pirt local context
  \return ierr   - PetscErrorCode

*/
PetscErrorCode joint_is_init(pirt_local_ctx *pirt_l){

  PetscInt       issize,isstart;
  PetscInt       low,high;
  PetscErrorCode ierr;
  PetscFunctionBeginUser;

  ierr    = VecGetOwnershipRange(pirt_l->recon_sample, &low, &high);CHKERRQ(ierr);
  isstart = low + pirt_l->ntheta_local;
  issize  = (high - low) - pirt_l->ntheta_local;
  ierr    = ISCreateStride(pirt_l->tasksubcomm, issize, isstart, 1, &(pirt_l->joint_sample_is));CHKERRQ(ierr);

  isstart = low;
  issize  = pirt_l->ntheta_local ;
  ierr    = ISCreateStride(pirt_l->tasksubcomm, issize, isstart, 1, &(pirt_l->joint_shifts_is));CHKERRQ(ierr);

  PetscFunctionReturn(ierr);
}

