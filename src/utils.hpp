#ifndef UTILS_H
#define UTILS_H

#include <petscviewerhdf5.h>
#include "pirtctx.hpp"
#include "subcomm.hpp"
#include "io.hpp"
#include "geom.hpp"

PetscErrorCode initialize_pirt(int argc, char **args, pirt_global_ctx *pirt_g, pirt_local_ctx *pirt_l, detctx *det);
PetscErrorCode finalize_pirt(pirt_global_ctx *pirt_g, pirt_local_ctx *pirt_l);

PetscErrorCode joint_is_init(pirt_local_ctx *pirt_l);

#endif
