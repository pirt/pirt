## Python script to generate tomography projection matrix and sinogram using 
## the astra toolbox. Arguments: size of object and number of projection angles

import astra
import numpy as np
import sys

size   = int(sys.argv[1])
angles = int(sys.argv[2])

# Test phantom dimensions -> (size, size)
# create a projection geometry describing this in astra.
vol_geom = astra.create_vol_geom(size,size)
proj_geom = astra.create_proj_geom('parallel', 1.0, size, np.linspace(1*(np.pi/180),360*(np.pi/180),angles))

# Create a projector
proj_id = astra.create_projector('line', proj_geom, vol_geom)

# Create the projection matrix
matrix_id = astra.projector.matrix(proj_id)

# Get the projection matrix as a Scipy sparse matrix.
W = astra.matrix.get(matrix_id)

# Save W
from scipy import sparse
sparse.save_npz("W_"+str(size)+"_"+str(angles)+".npz", W)

# Generate a sinogram using the projection matrix and save it
import scipy.io
P = np.load("softwood_"+str(size)+".npy")
s = W.dot(P.ravel())
s = np.reshape(s, (len(proj_geom['ProjectionAngles']),proj_geom['DetectorCount']))
np.save("astrasino_"+str(size)+"_"+str(angles)+".npy", s)

# cleanup
astra.projector.delete(proj_id)
astra.matrix.delete(matrix_id)
