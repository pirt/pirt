## python script to generate sinogram and projection angles input files
## Arguments: size, number of projection angles

import numpy as np
import h5py, os, sys

## Params
ntau        = int(sys.argv[1])
ntheta      = int(sys.argv[2])

angles   = np.linspace(1,360,ntheta)
sinogram = np.load("astrasino_"+str(ntau)+"_"+str(ntheta)+".npy").reshape(ntheta*ntau)

## Generate anglesfile
try:
    os.remove("theta_"+str(ntheta)+".h5")
except:
    pass
f = h5py.File("theta_"+str(ntheta)+".h5",mode="w")
grp1  = f.create_group("theta")
dset1 = f.create_dataset("/theta/angles", (ntheta,), dtype="f8")
dset1[:] = angles
f.close()

## Generate sinofile
try:
    os.remove("softwood_"+str(ntau)+".h5")
except:
    pass
f = h5py.File("softwood_"+str(ntau)+".h5",mode="w")
grp2  = f.create_group("sino0")
dset2 = f.create_dataset("/sino0/sinogram", (ntheta*ntau,), dtype="f8")
dset2[:]  = sinogram
f.close()

