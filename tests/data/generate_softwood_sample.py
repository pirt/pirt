## python Script to generate softwood sample using xdesign, 
## arguments: size of sample to be generated 

from xdesign import *
import matplotlib.pyplot as plt
import numpy as np
import sys

## Params
SIZE = int(sys.argv[1])
np.random.seed(0) # random seed for repeatability

## Generate softoowd sample and save as .npy
p1 = Softwood()
d1 = discrete_phantom(p1, SIZE)
np.save("softwood_"+str(SIZE)+".npy",d1)
