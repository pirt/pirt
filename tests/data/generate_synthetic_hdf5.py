## python script to generate synthetic.h5 sample input
## and theta_30.h5 projection angles input

import numpy as np
import h5py,os

## Read sample.dat
f = open("sample.dat")
l = f.readlines()
data = np.zeros((100,100))
for i in range(len(l)):
    p,q,r = l[i].split()[:]
    p = int(p)
    q = int(q)
    r = float(r)
    data[p-1][q-1] = r
f.close()

## Params
nTau   = int(data.shape[0])
nTheta = int(25)
angles = np.linspace(1.0,360.0,nTheta, dtype = np.float64)

## Generate anglesfile
os.remove('theta_25.h5')
f = h5py.File("theta_25.h5",mode="w")
grp1  = f.create_group("theta")
dset1 = f.create_dataset("/theta/angles", (nTheta,), dtype='f8')
dset1[:] = angles
f.close()

## Generate synthetic file
os.remove('synthetic.h5')
f = h5py.File("synthetic.h5",mode="w")
grp2  = f.create_group("sample")
dset2 = f.create_dataset("/sample/input", (nTau*nTau,), dtype='f8')
dset2[:]  = data.flatten()
f.close()