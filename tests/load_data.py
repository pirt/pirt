## UItilities to load data from bin, hdf5 and npy files

import numpy as np
import matplotlib.pyplot as plt
from petsc4py import PETSc
from scipy import sparse
import h5py

def load_matrices(pirtmatfile = str('data/A.bin'), astramatfile = str('data/W_64_30.npz')):

    viewer = PETSc.Viewer().createBinary(pirtmatfile, 'r')
    A1 = PETSc.Mat().load(viewer)
    pirt_i, pirt_j, pirt_v = A1.getValuesCSR()
    pirt_mat  = sparse.csr_matrix((pirt_v,pirt_j,pirt_i), A1.size)
    pirt_mat_ = sparse.csr_matrix((np.array(pirt_v/np.max(pirt_v)),pirt_j,pirt_i), A1.size)
    A1.destroy
    viewer.destroy()
    pirt_mat  = sparse.csr_matrix((pirt_v,pirt_j,pirt_i), A1.size)

    astra_mat = sparse.load_npz(astramatfile)
    astra_i = astra_mat.indptr
    astra_j = astra_mat.indices
    astra_v = astra_mat.data

    return pirt_i, pirt_j, pirt_v, pirt_mat, astra_i, astra_j, astra_v, astra_mat

def load_sample(samplefile = str('data/softwood_512.npy')):
    
    return np.load(samplefile)

def load_recon(samplefile = str('data/sol_noeccor.h5')):

    f = h5py.File(samplefile ,'r')
    recon_dset = f['reconstruction/solution']
    recon = np.zeros(recon_dset.shape)
    recon_dset.read_direct(recon,np.s_[:],np.s_[:])
    f.close()
    
    return recon
