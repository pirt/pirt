from load_data import load_matrices
import numpy as np
from skimage.metrics import structural_similarity as ssim

# content of test_class.py
class TestMatrix:
    
    # Test number of non zeros
    def test_matrix_nnz(self) : 
        pirt_i, pirt_j, pirt_v, pirt_mat, astra_i, astra_j, astra_v, astra_mat = load_matrices() 
        assert (int(pirt_i.shape[0])/int(astra_i.shape[0]) - 1) < 0.05
        
    # Test for dimensions    
    def test_matrix_i(self) :
        pirt_i, pirt_j, pirt_v, pirt_mat, astra_i, astra_j, astra_v, astra_mat = load_matrices()  
        assert (int(pirt_i.shape[0])/int(astra_i.shape[0]) - 1) < 0.05    

    # Test for dimensions    
    def test_matrix_j(self) : 
        pirt_i, pirt_j, pirt_v, pirt_mat, astra_i, astra_j, astra_v, astra_mat = load_matrices()  
        assert (int(pirt_j.shape[0])/int(astra_j.shape[0]) - 1) < 0.05    
        
    # Test sparsity pattern
    # SSIM is a creative hack, for now
    def test_sparsity_pattern(self):
        pirt_i, pirt_j, pirt_v, pirt_mat, astra_i, astra_j, astra_v, astra_mat = load_matrices()  
        assert ssim( (pirt_mat/np.max(pirt_mat)).toarray(), astra_mat.toarray()) > 0.9