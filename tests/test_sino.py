from load_data import load_sample
from load_data import load_matrices
import numpy as np

# Test sinogram
class TestSino:
    
    def test_sino(self) : 
        sample = load_sample('data/softwood_64.npy')
        pirt_i, pirt_j, pirt_v, pirt_mat, astra_i, astra_j, astra_v, astra_mat = load_matrices()  
        sino_pirt  = (pirt_mat/np.max(pirt_mat)).dot(sample.flatten())
        sino_astra = astra_mat.dot(sample.flatten())
        diff = np.array(np.abs(sino_pirt.flatten() - sino_astra.flatten()))
        assert np.sqrt(np.sum(diff**2))/np.size(diff) < 0.1
        
