from load_data import load_sample
from load_data import load_recon
import numpy as np
from skimage.registration import phase_cross_correlation

# Test reconstructions of softwood phantom
class TestSoftwoodRecon:
    
    def test_noeccor(self) : 
        recon = load_recon('data/sol_noeccor.h5').reshape(512,512)
        ref   = load_sample().reshape(512,512)
        shifts, error, phasediff = phase_cross_correlation(recon,ref)
        assert error < 0.5
        
    def test_joint(self) : 
        recon = load_recon('data/sol_joint.h5').reshape(512,512)
        ref   = load_sample().reshape(512,512)
        shifts, error, phasediff = phase_cross_correlation(recon,ref)
        assert error < 0.5
                
    def test_alt(self) : 
        recon = load_recon('data/sol_alt.h5').reshape(512,512)
        ref   = load_sample().reshape(512,512)
        shifts, error, phasediff = phase_cross_correlation(recon,ref)
        assert error < 0.5
        
